﻿namespace SisMatriculaENGSC.Vistas
{
    partial class frmMenuMatricula
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenuMatricula));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblFechaHora = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelVirtualUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelUserVirtual = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDelSistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cambiarModuloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moduloMatriculaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moduloNotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSeciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btbCatedraticos = new System.Windows.Forms.Button();
            this.btnEncargados = new System.Windows.Forms.Button();
            this.btnUsuarios = new System.Windows.Forms.Button();
            this.btnMatriculas = new System.Windows.Forms.Button();
            this.btnAlumnos = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.axShockwaveFlash2 = new AxShockwaveFlashObjects.AxShockwaveFlash();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axShockwaveFlash2)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblUsuario,
            this.lblFechaHora,
            this.toolStripStatusLabelVirtualUser,
            this.labelUserVirtual});
            this.statusStrip1.Location = new System.Drawing.Point(0, 968);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1657, 25);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblUsuario
            // 
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(49, 20);
            this.lblUsuario.Text = "Status";
            this.lblUsuario.Click += new System.EventHandler(this.lblUsuario_Click);
            // 
            // lblFechaHora
            // 
            this.lblFechaHora.Name = "lblFechaHora";
            this.lblFechaHora.Size = new System.Drawing.Size(84, 20);
            this.lblFechaHora.Text = "Fecha Hora";
            this.lblFechaHora.Click += new System.EventHandler(this.lblFechaHora_Click);
            // 
            // toolStripStatusLabelVirtualUser
            // 
            this.toolStripStatusLabelVirtualUser.Name = "toolStripStatusLabelVirtualUser";
            this.toolStripStatusLabelVirtualUser.Size = new System.Drawing.Size(0, 20);
            // 
            // labelUserVirtual
            // 
            this.labelUserVirtual.Name = "labelUserVirtual";
            this.labelUserVirtual.Size = new System.Drawing.Size(106, 20);
            this.labelUserVirtual.Text = "Usuario Virtual";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(675, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(422, 48);
            this.label1.TabIndex = 19;
            this.label1.Text = "Modulo de Matriculas";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.herramientasToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.acercaDelSistemaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1657, 28);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(110, 24);
            this.herramientasToolStripMenuItem.Text = "Herramientas";
            this.herramientasToolStripMenuItem.Click += new System.EventHandler(this.herramientasToolStripMenuItem_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Enabled = false;
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(77, 24);
            this.usuariosToolStripMenuItem.Text = "&Usuarios";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.usuariosToolStripMenuItem_Click);
            // 
            // acercaDelSistemaToolStripMenuItem
            // 
            this.acercaDelSistemaToolStripMenuItem.Name = "acercaDelSistemaToolStripMenuItem";
            this.acercaDelSistemaToolStripMenuItem.Size = new System.Drawing.Size(147, 24);
            this.acercaDelSistemaToolStripMenuItem.Text = "Acerca del Sistema";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.cambiarModuloToolStripMenuItem,
            this.cerrarSeciToolStripMenuItem,
            this.salirToolStripMenuItem1});
            this.toolStripMenuItem1.Image = global::SisMatriculaENGSC.Properties.Resources.usuario1;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(32, 24);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(193, 6);
            // 
            // cambiarModuloToolStripMenuItem
            // 
            this.cambiarModuloToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moduloMatriculaToolStripMenuItem,
            this.moduloNotasToolStripMenuItem});
            this.cambiarModuloToolStripMenuItem.Name = "cambiarModuloToolStripMenuItem";
            this.cambiarModuloToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.cambiarModuloToolStripMenuItem.Text = "Cambiar Modulo";
            // 
            // moduloMatriculaToolStripMenuItem
            // 
            this.moduloMatriculaToolStripMenuItem.Name = "moduloMatriculaToolStripMenuItem";
            this.moduloMatriculaToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.moduloMatriculaToolStripMenuItem.Text = "Modulo Matricula";
            this.moduloMatriculaToolStripMenuItem.Click += new System.EventHandler(this.moduloMatriculaToolStripMenuItem_Click);
            // 
            // moduloNotasToolStripMenuItem
            // 
            this.moduloNotasToolStripMenuItem.Name = "moduloNotasToolStripMenuItem";
            this.moduloNotasToolStripMenuItem.Size = new System.Drawing.Size(202, 26);
            this.moduloNotasToolStripMenuItem.Text = "Modulo Notas";
            this.moduloNotasToolStripMenuItem.Click += new System.EventHandler(this.moduloNotasToolStripMenuItem_Click);
            // 
            // cerrarSeciToolStripMenuItem
            // 
            this.cerrarSeciToolStripMenuItem.Name = "cerrarSeciToolStripMenuItem";
            this.cerrarSeciToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.cerrarSeciToolStripMenuItem.Text = "Cerrar Sesión";
            this.cerrarSeciToolStripMenuItem.Click += new System.EventHandler(this.cerrarSeciToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem1
            // 
            this.salirToolStripMenuItem1.Name = "salirToolStripMenuItem1";
            this.salirToolStripMenuItem1.Size = new System.Drawing.Size(196, 26);
            this.salirToolStripMenuItem1.Text = "Salir";
            this.salirToolStripMenuItem1.Click += new System.EventHandler(this.salirToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(83, 24);
            this.toolStripMenuItem2.Text = "Ayuda";
            // 
            // btbCatedraticos
            // 
            this.btbCatedraticos.AccessibleDescription = "";
            this.btbCatedraticos.AccessibleName = "";
            this.btbCatedraticos.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.icono_tutoria;
            this.btbCatedraticos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btbCatedraticos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btbCatedraticos.Enabled = false;
            this.btbCatedraticos.FlatAppearance.BorderSize = 0;
            this.btbCatedraticos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btbCatedraticos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btbCatedraticos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbCatedraticos.ForeColor = System.Drawing.Color.SeaGreen;
            this.btbCatedraticos.Location = new System.Drawing.Point(760, 546);
            this.btbCatedraticos.Margin = new System.Windows.Forms.Padding(4);
            this.btbCatedraticos.Name = "btbCatedraticos";
            this.btbCatedraticos.Size = new System.Drawing.Size(291, 266);
            this.btbCatedraticos.TabIndex = 18;
            this.btbCatedraticos.Tag = "";
            this.btbCatedraticos.Text = "Catedraticos";
            this.btbCatedraticos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btbCatedraticos, "Catedraticos");
            this.btbCatedraticos.UseVisualStyleBackColor = true;
            this.btbCatedraticos.Click += new System.EventHandler(this.btbCatedraticos_Click);
            // 
            // btnEncargados
            // 
            this.btnEncargados.AccessibleDescription = "";
            this.btnEncargados.AccessibleName = "";
            this.btnEncargados.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.alumnos;
            this.btnEncargados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEncargados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEncargados.Enabled = false;
            this.btnEncargados.FlatAppearance.BorderSize = 0;
            this.btnEncargados.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnEncargados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEncargados.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEncargados.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnEncargados.Location = new System.Drawing.Point(1112, 459);
            this.btnEncargados.Margin = new System.Windows.Forms.Padding(4);
            this.btnEncargados.Name = "btnEncargados";
            this.btnEncargados.Size = new System.Drawing.Size(291, 270);
            this.btnEncargados.TabIndex = 17;
            this.btnEncargados.Tag = "";
            this.btnEncargados.Text = "Encargados";
            this.btnEncargados.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnEncargados, "Encargados");
            this.btnEncargados.UseVisualStyleBackColor = true;
            this.btnEncargados.Click += new System.EventHandler(this.btnEncargados_Click);
            // 
            // btnUsuarios
            // 
            this.btnUsuarios.AccessibleDescription = "";
            this.btnUsuarios.AccessibleName = "";
            this.btnUsuarios.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.notamodulos;
            this.btnUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUsuarios.FlatAppearance.BorderSize = 0;
            this.btnUsuarios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsuarios.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnUsuarios.Location = new System.Drawing.Point(1196, 121);
            this.btnUsuarios.Margin = new System.Windows.Forms.Padding(4);
            this.btnUsuarios.Name = "btnUsuarios";
            this.btnUsuarios.Size = new System.Drawing.Size(291, 270);
            this.btnUsuarios.TabIndex = 15;
            this.btnUsuarios.Tag = "";
            this.btnUsuarios.Text = "Consultas";
            this.btnUsuarios.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnUsuarios, "Consultar");
            this.btnUsuarios.UseVisualStyleBackColor = true;
            this.btnUsuarios.Click += new System.EventHandler(this.btnUsuarios_Click);
            // 
            // btnMatriculas
            // 
            this.btnMatriculas.AccessibleDescription = "";
            this.btnMatriculas.AccessibleName = "";
            this.btnMatriculas.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.matricula;
            this.btnMatriculas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMatriculas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMatriculas.Enabled = false;
            this.btnMatriculas.FlatAppearance.BorderSize = 0;
            this.btnMatriculas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMatriculas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMatriculas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMatriculas.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnMatriculas.Location = new System.Drawing.Point(292, 121);
            this.btnMatriculas.Margin = new System.Windows.Forms.Padding(4);
            this.btnMatriculas.Name = "btnMatriculas";
            this.btnMatriculas.Size = new System.Drawing.Size(291, 270);
            this.btnMatriculas.TabIndex = 14;
            this.btnMatriculas.Tag = "";
            this.btnMatriculas.Text = "Matriculas";
            this.btnMatriculas.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnMatriculas, "Matricular");
            this.btnMatriculas.UseVisualStyleBackColor = true;
            this.btnMatriculas.Click += new System.EventHandler(this.btnMatriculas_Click);
            // 
            // btnAlumnos
            // 
            this.btnAlumnos.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.alumnos2;
            this.btnAlumnos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAlumnos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlumnos.Enabled = false;
            this.btnAlumnos.FlatAppearance.BorderSize = 0;
            this.btnAlumnos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnAlumnos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlumnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlumnos.ForeColor = System.Drawing.Color.SeaGreen;
            this.btnAlumnos.Location = new System.Drawing.Point(376, 459);
            this.btnAlumnos.Margin = new System.Windows.Forms.Padding(4);
            this.btnAlumnos.Name = "btnAlumnos";
            this.btnAlumnos.Size = new System.Drawing.Size(291, 270);
            this.btnAlumnos.TabIndex = 13;
            this.btnAlumnos.Tag = "";
            this.btnAlumnos.Text = "Alumnos";
            this.btnAlumnos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnAlumnos, "Alumnos");
            this.btnAlumnos.UseVisualStyleBackColor = true;
            this.btnAlumnos.Click += new System.EventHandler(this.btnAlumnos_Click);
            // 
            // axShockwaveFlash2
            // 
            this.axShockwaveFlash2.Enabled = true;
            this.axShockwaveFlash2.Location = new System.Drawing.Point(531, 90);
            this.axShockwaveFlash2.Margin = new System.Windows.Forms.Padding(4);
            this.axShockwaveFlash2.Name = "axShockwaveFlash2";
            this.axShockwaveFlash2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axShockwaveFlash2.OcxState")));
            this.axShockwaveFlash2.Size = new System.Drawing.Size(300, 332);
            this.axShockwaveFlash2.TabIndex = 21;
            // 
            // frmMenuMatricula
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1657, 993);
            this.Controls.Add(this.axShockwaveFlash2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btbCatedraticos);
            this.Controls.Add(this.btnEncargados);
            this.Controls.Add(this.btnUsuarios);
            this.Controls.Add(this.btnMatriculas);
            this.Controls.Add(this.btnAlumnos);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMenuMatricula";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Matricula";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMenuMatricula_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axShockwaveFlash2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblUsuario;
        private System.Windows.Forms.ToolStripStatusLabel lblFechaHora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDelSistemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem cambiarModuloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moduloMatriculaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moduloNotasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSeciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        public System.Windows.Forms.Button btnMatriculas;
        public System.Windows.Forms.Button btbCatedraticos;
        public System.Windows.Forms.Button btnEncargados;
        public System.Windows.Forms.Button btnUsuarios;
        public System.Windows.Forms.Button btnAlumnos;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelVirtualUser;
        private System.Windows.Forms.ToolStripStatusLabel labelUserVirtual;
        private System.Windows.Forms.ToolTip toolTip1;
        public AxShockwaveFlashObjects.AxShockwaveFlash axShockwaveFlash2;
    }
}