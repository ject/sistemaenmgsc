﻿namespace SisMatriculaENGSC.Vistas
{
    partial class frmMatriculas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMatriculas));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtJornada = new System.Windows.Forms.TextBox();
            this.txtCarrera = new System.Windows.Forms.TextBox();
            this.txtCurso = new System.Windows.Forms.TextBox();
            this.txtSemestre = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.opcionV = new System.Windows.Forms.RadioButton();
            this.opcionM = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.opcionSemestre2 = new System.Windows.Forms.RadioButton();
            this.opcionSemestre1 = new System.Windows.Forms.RadioButton();
            this.lblAño = new System.Windows.Forms.Label();
            this.dateMatricula = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.comboCondicion = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboCurso = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboCarrera = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBuscarAlumno = new System.Windows.Forms.Button();
            this.txtIdAlumno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.radioBuscarM = new System.Windows.Forms.RadioButton();
            this.radioBuscarA = new System.Windows.Forms.RadioButton();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.comboBuscar = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gridbusqueda = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSeccion = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridbusqueda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-1, 65);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(777, 422);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(769, 393);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Matricula";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtSeccion);
            this.groupBox1.Controls.Add(this.txtJornada);
            this.groupBox1.Controls.Add(this.txtCarrera);
            this.groupBox1.Controls.Add(this.txtCurso);
            this.groupBox1.Controls.Add(this.txtSemestre);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.lblAño);
            this.groupBox1.Controls.Add(this.dateMatricula);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnNuevo);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Controls.Add(this.comboCondicion);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboCurso);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboCarrera);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnBuscarAlumno);
            this.groupBox1.Controls.Add(this.txtIdAlumno);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(759, 386);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos de Matricula";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtJornada
            // 
            this.txtJornada.Location = new System.Drawing.Point(440, 261);
            this.txtJornada.Name = "txtJornada";
            this.txtJornada.Size = new System.Drawing.Size(100, 22);
            this.txtJornada.TabIndex = 61;
            this.txtJornada.Visible = false;
            // 
            // txtCarrera
            // 
            this.txtCarrera.Location = new System.Drawing.Point(437, 109);
            this.txtCarrera.Name = "txtCarrera";
            this.txtCarrera.Size = new System.Drawing.Size(100, 22);
            this.txtCarrera.TabIndex = 61;
            this.txtCarrera.Visible = false;
            // 
            // txtCurso
            // 
            this.txtCurso.Location = new System.Drawing.Point(437, 156);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.Size = new System.Drawing.Size(100, 22);
            this.txtCurso.TabIndex = 61;
            this.txtCurso.Visible = false;
            // 
            // txtSemestre
            // 
            this.txtSemestre.Location = new System.Drawing.Point(440, 203);
            this.txtSemestre.Name = "txtSemestre";
            this.txtSemestre.Size = new System.Drawing.Size(100, 22);
            this.txtSemestre.TabIndex = 61;
            this.txtSemestre.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 261);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 17);
            this.label8.TabIndex = 60;
            this.label8.Text = "Jornada";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.opcionV);
            this.groupBox3.Controls.Add(this.opcionM);
            this.groupBox3.Location = new System.Drawing.Point(165, 245);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(268, 50);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            // 
            // opcionV
            // 
            this.opcionV.AutoSize = true;
            this.opcionV.Enabled = false;
            this.opcionV.Location = new System.Drawing.Point(123, 16);
            this.opcionV.Margin = new System.Windows.Forms.Padding(4);
            this.opcionV.Name = "opcionV";
            this.opcionV.Size = new System.Drawing.Size(97, 21);
            this.opcionV.TabIndex = 1;
            this.opcionV.TabStop = true;
            this.opcionV.Text = "Vespertina";
            this.opcionV.UseVisualStyleBackColor = true;
            this.opcionV.CheckedChanged += new System.EventHandler(this.opcionV_CheckedChanged);
            // 
            // opcionM
            // 
            this.opcionM.AutoSize = true;
            this.opcionM.Enabled = false;
            this.opcionM.Location = new System.Drawing.Point(8, 16);
            this.opcionM.Margin = new System.Windows.Forms.Padding(4);
            this.opcionM.Name = "opcionM";
            this.opcionM.Size = new System.Drawing.Size(83, 21);
            this.opcionM.TabIndex = 0;
            this.opcionM.TabStop = true;
            this.opcionM.Text = "Matutina";
            this.opcionM.UseVisualStyleBackColor = true;
            this.opcionM.CheckedChanged += new System.EventHandler(this.opcionM_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.opcionSemestre2);
            this.groupBox2.Controls.Add(this.opcionSemestre1);
            this.groupBox2.Location = new System.Drawing.Point(164, 187);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(268, 50);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            // 
            // opcionSemestre2
            // 
            this.opcionSemestre2.AutoSize = true;
            this.opcionSemestre2.Enabled = false;
            this.opcionSemestre2.Location = new System.Drawing.Point(124, 17);
            this.opcionSemestre2.Margin = new System.Windows.Forms.Padding(4);
            this.opcionSemestre2.Name = "opcionSemestre2";
            this.opcionSemestre2.Size = new System.Drawing.Size(86, 21);
            this.opcionSemestre2.TabIndex = 1;
            this.opcionSemestre2.TabStop = true;
            this.opcionSemestre2.Text = "Segundo";
            this.opcionSemestre2.UseVisualStyleBackColor = true;
            this.opcionSemestre2.CheckedChanged += new System.EventHandler(this.opcionSemestre2_CheckedChanged);
            // 
            // opcionSemestre1
            // 
            this.opcionSemestre1.AutoSize = true;
            this.opcionSemestre1.Enabled = false;
            this.opcionSemestre1.Location = new System.Drawing.Point(8, 16);
            this.opcionSemestre1.Margin = new System.Windows.Forms.Padding(4);
            this.opcionSemestre1.Name = "opcionSemestre1";
            this.opcionSemestre1.Size = new System.Drawing.Size(78, 21);
            this.opcionSemestre1.TabIndex = 0;
            this.opcionSemestre1.TabStop = true;
            this.opcionSemestre1.Text = "Primero";
            this.opcionSemestre1.UseVisualStyleBackColor = true;
            this.opcionSemestre1.CheckedChanged += new System.EventHandler(this.opcionSemestre1_CheckedChanged);
            // 
            // lblAño
            // 
            this.lblAño.AutoSize = true;
            this.lblAño.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAño.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAño.Location = new System.Drawing.Point(664, 11);
            this.lblAño.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAño.Name = "lblAño";
            this.lblAño.Size = new System.Drawing.Size(95, 22);
            this.lblAño.TabIndex = 57;
            this.lblAño.Text = "Año: 2015";
            // 
            // dateMatricula
            // 
            this.dateMatricula.Enabled = false;
            this.dateMatricula.Location = new System.Drawing.Point(165, 346);
            this.dateMatricula.Margin = new System.Windows.Forms.Padding(4);
            this.dateMatricula.Name = "dateMatricula";
            this.dateMatricula.Size = new System.Drawing.Size(265, 22);
            this.dateMatricula.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 346);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 17);
            this.label6.TabIndex = 49;
            this.label6.Text = "Fecha de Matricula";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(587, 112);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(4);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(127, 70);
            this.btnNuevo.TabIndex = 47;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Location = new System.Drawing.Point(587, 255);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(127, 70);
            this.btnGuardar.TabIndex = 46;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // comboCondicion
            // 
            this.comboCondicion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCondicion.Enabled = false;
            this.comboCondicion.FormattingEnabled = true;
            this.comboCondicion.Items.AddRange(new object[] {
            "Primer Ingreso",
            "Reingreso",
            "Repitente"});
            this.comboCondicion.Location = new System.Drawing.Point(164, 302);
            this.comboCondicion.Margin = new System.Windows.Forms.Padding(4);
            this.comboCondicion.Name = "comboCondicion";
            this.comboCondicion.Size = new System.Drawing.Size(263, 24);
            this.comboCondicion.TabIndex = 42;
            this.comboCondicion.SelectedIndexChanged += new System.EventHandler(this.comboCondicion_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 305);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 17);
            this.label5.TabIndex = 41;
            this.label5.Text = "Condición de Ingreso";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 203);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 39;
            this.label4.Text = "Semestre";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // comboCurso
            // 
            this.comboCurso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCurso.Enabled = false;
            this.comboCurso.FormattingEnabled = true;
            this.comboCurso.Location = new System.Drawing.Point(167, 154);
            this.comboCurso.Margin = new System.Windows.Forms.Padding(4);
            this.comboCurso.Name = "comboCurso";
            this.comboCurso.Size = new System.Drawing.Size(263, 24);
            this.comboCurso.TabIndex = 38;
            this.comboCurso.SelectedIndexChanged += new System.EventHandler(this.comboCurso_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 158);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "Curso";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // comboCarrera
            // 
            this.comboCarrera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCarrera.Enabled = false;
            this.comboCarrera.FormattingEnabled = true;
            this.comboCarrera.Items.AddRange(new object[] {
            "Magisterio",
            "Bachiller en Humanidades"});
            this.comboCarrera.Location = new System.Drawing.Point(167, 108);
            this.comboCarrera.Margin = new System.Windows.Forms.Padding(4);
            this.comboCarrera.Name = "comboCarrera";
            this.comboCarrera.Size = new System.Drawing.Size(263, 24);
            this.comboCarrera.TabIndex = 36;
            this.comboCarrera.SelectedIndexChanged += new System.EventHandler(this.comboCarrera_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 112);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 35;
            this.label2.Text = "Carrera";
            // 
            // btnBuscarAlumno
            // 
            this.btnBuscarAlumno.Enabled = false;
            this.btnBuscarAlumno.Location = new System.Drawing.Point(440, 57);
            this.btnBuscarAlumno.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuscarAlumno.Name = "btnBuscarAlumno";
            this.btnBuscarAlumno.Size = new System.Drawing.Size(115, 34);
            this.btnBuscarAlumno.TabIndex = 34;
            this.btnBuscarAlumno.Text = "Buscar Alumno";
            this.btnBuscarAlumno.UseVisualStyleBackColor = true;
            this.btnBuscarAlumno.Click += new System.EventHandler(this.btnBuscarEncargado_Click);
            // 
            // txtIdAlumno
            // 
            this.txtIdAlumno.Location = new System.Drawing.Point(164, 63);
            this.txtIdAlumno.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdAlumno.Name = "txtIdAlumno";
            this.txtIdAlumno.Size = new System.Drawing.Size(267, 22);
            this.txtIdAlumno.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 66);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Identidad Alumno";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.radioBuscarM);
            this.tabPage2.Controls.Add(this.radioBuscarA);
            this.tabPage2.Controls.Add(this.btnEliminar);
            this.tabPage2.Controls.Add(this.btnEditar);
            this.tabPage2.Controls.Add(this.btnBuscar);
            this.tabPage2.Controls.Add(this.txtBuscar);
            this.tabPage2.Controls.Add(this.comboBuscar);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.gridbusqueda);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(769, 393);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consulta";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // radioBuscarM
            // 
            this.radioBuscarM.AutoSize = true;
            this.radioBuscarM.Location = new System.Drawing.Point(399, 19);
            this.radioBuscarM.Name = "radioBuscarM";
            this.radioBuscarM.Size = new System.Drawing.Size(86, 21);
            this.radioBuscarM.TabIndex = 7;
            this.radioBuscarM.TabStop = true;
            this.radioBuscarM.Text = "Matricula";
            this.radioBuscarM.UseVisualStyleBackColor = true;
            this.radioBuscarM.CheckedChanged += new System.EventHandler(this.radioBuscar_CheckedChanged);
            // 
            // radioBuscarA
            // 
            this.radioBuscarA.AutoSize = true;
            this.radioBuscarA.Location = new System.Drawing.Point(223, 19);
            this.radioBuscarA.Name = "radioBuscarA";
            this.radioBuscarA.Size = new System.Drawing.Size(83, 21);
            this.radioBuscarA.TabIndex = 7;
            this.radioBuscarA.TabStop = true;
            this.radioBuscarA.Text = "Alumnos";
            this.radioBuscarA.UseVisualStyleBackColor = true;
            this.radioBuscarA.CheckedChanged += new System.EventHandler(this.radioBuscar_CheckedChanged);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(463, 351);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(100, 28);
            this.btnEliminar.TabIndex = 6;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Visible = false;
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(184, 351);
            this.btnEditar.Margin = new System.Windows.Forms.Padding(4);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(100, 28);
            this.btnEditar.TabIndex = 5;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Visible = false;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Enabled = false;
            this.btnBuscar.Location = new System.Drawing.Point(608, 61);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(100, 28);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Enabled = false;
            this.txtBuscar.Location = new System.Drawing.Point(408, 63);
            this.txtBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(191, 22);
            this.txtBuscar.TabIndex = 3;
            // 
            // comboBuscar
            // 
            this.comboBuscar.Enabled = false;
            this.comboBuscar.FormattingEnabled = true;
            this.comboBuscar.Items.AddRange(new object[] {
            "Carrera",
            "Curso ",
            "Identidad",
            "Semestre"});
            this.comboBuscar.Location = new System.Drawing.Point(145, 62);
            this.comboBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.comboBuscar.Name = "comboBuscar";
            this.comboBuscar.Size = new System.Drawing.Size(191, 24);
            this.comboBuscar.TabIndex = 2;
            this.comboBuscar.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 67);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Buscar por";
            // 
            // gridbusqueda
            // 
            this.gridbusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridbusqueda.Location = new System.Drawing.Point(0, 105);
            this.gridbusqueda.Margin = new System.Windows.Forms.Padding(4);
            this.gridbusqueda.Name = "gridbusqueda";
            this.gridbusqueda.Size = new System.Drawing.Size(763, 240);
            this.gridbusqueda.TabIndex = 0;
            this.gridbusqueda.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridbusqueda_CellContentClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::SisMatriculaENGSC.Properties.Resources.close;
            this.pictureBox1.Location = new System.Drawing.Point(737, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(203, 11);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(361, 48);
            this.label15.TabIndex = 55;
            this.label15.Text = "Matricular Alumno";
            // 
            // txtSeccion
            // 
            this.txtSeccion.Location = new System.Drawing.Point(440, 343);
            this.txtSeccion.Name = "txtSeccion";
            this.txtSeccion.Size = new System.Drawing.Size(100, 22);
            this.txtSeccion.TabIndex = 61;
            this.txtSeccion.Visible = false;
            // 
            // frmMatriculas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(777, 484);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMatriculas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Matricula";
            this.Load += new System.EventHandler(this.frmMatricula_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridbusqueda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtIdAlumno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboCurso;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboCarrera;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnBuscarAlumno;
        private System.Windows.Forms.ComboBox comboCondicion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.DateTimePicker dateMatricula;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView gridbusqueda;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.ComboBox comboBuscar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblAño;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton opcionV;
        private System.Windows.Forms.RadioButton opcionM;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton opcionSemestre2;
        private System.Windows.Forms.RadioButton opcionSemestre1;
        private System.Windows.Forms.RadioButton radioBuscarA;
        private System.Windows.Forms.RadioButton radioBuscarM;
        private System.Windows.Forms.TextBox txtJornada;
        private System.Windows.Forms.TextBox txtSemestre;
        private System.Windows.Forms.TextBox txtCarrera;
        private System.Windows.Forms.TextBox txtCurso;
        private System.Windows.Forms.TextBox txtSeccion;
    }
}