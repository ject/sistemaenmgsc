﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmCertificados : Form
    {
        public frmCertificados()
        {
            InitializeComponent();
        }

        private void btnReporteMatricula_Click(object sender, EventArgs e)
        {
            this.Close();
            Vistas.frmCertificadoConducta frm = new Vistas.frmCertificadoConducta();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Vistas.frmCertificadoEstudio frm = new Vistas.frmCertificadoEstudio();
            frm.Show();
        }
    }
}
