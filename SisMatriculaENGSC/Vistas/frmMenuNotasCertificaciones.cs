﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmMenuNotasCertificaciones : Form
    {
        public frmMenuNotasCertificaciones()
        {
            InitializeComponent();
        }

        private void alumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vistas.frmAlumnos frm = new Vistas.frmAlumnos();
            frm.Show();

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea salir del Sistema?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();

            }


        }

        private void gestionDePersonasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Vistas.frmAlumnos   frm = new Vistas.frmAlumnos  ();
            frm.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblFechaHora.Text = System.DateTime.Now.ToString("   dd/MM/yyyy    hh:mm:ss");
        }

        private void btnEncargados_Click(object sender, EventArgs e)
        {
            Vistas.frmEncargados frm = new Vistas.frmEncargados();
            frm.Show();
        }

        private void btbCatedraticos_Click(object sender, EventArgs e)
        {
            Vistas.frmCatedraticos frm = new Vistas.frmCatedraticos();
            frm.Show();
        }

        private void encargadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vistas.frmEncargados frm = new Vistas.frmEncargados();
            frm.Show();
        }

        private void maestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vistas.frmCatedraticos frm = new Vistas.frmCatedraticos();
            frm.Show();
        }

        private void btnMatriculas_Click(object sender, EventArgs e)
        {
            Vistas.frmMatriculas frm = new Vistas.frmMatriculas();
            frm.Show();
        }

        private void matriculasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vistas.frmMatriculas frm = new Vistas.frmMatriculas();
            frm.Show();
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            Vistas.frmCertificados frm = new Vistas.frmCertificados();
            frm.Show();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            Vistas.frmUsuarios frm = new Vistas.frmUsuarios();
            frm.Show();
        }

        private void btnMaterias_Click(object sender, EventArgs e)
        {
            Vistas.frmMaterias frm = new Vistas.frmMaterias();
            frm.Show();
        }

        private void cerraSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea Cerrar Sesion?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Vistas.frmMenuNotasCertificaciones frm = new Vistas.frmMenuNotasCertificaciones();
                frm.Close();
                Vistas.frmLogin frm2 = new Vistas.frmLogin();
                frm2.Show();

            }
            

            
        }

        private void btnNotas_Click(object sender, EventArgs e)
        {
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea salir del Sistema?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();

            }
        }

        private void cerrarSeciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea Cerrar Sesión?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Vistas.frmMenuNotasCertificaciones frm = new Vistas.frmMenuNotasCertificaciones();
                frm.Close();
                Vistas.frmLogin frm2 = new Vistas.frmLogin();
                frm2.Show();

            }
        }

        private void cambiarModuloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (MessageBox.Show("¿Está seguro que desea Cambiar Modulo?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Hide();
                Vistas.frmModulos frm = new Vistas.frmModulos();
                frm.Show();
            }
        }

        private void combosDeMateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vistas.frmCombosMaterias frm = new Vistas.frmCombosMaterias();
            frm.Show();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Vistas.frmUsuarios frm = new Vistas.frmUsuarios();
            frm.Show();
        }

        private void moduloMatriculaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Vistas.frmMenuMatricula frm = new Vistas.frmMenuMatricula();
            frm.Show();
        }

        private void moduloNotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Vistas.frmMenuNotasCertificaciones frm = new Vistas.frmMenuNotasCertificaciones();
            frm.Show();
        }

        private void lblFechaHora_Click(object sender, EventArgs e)
        {

        }

        private void acercaDelSistemaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void herramientasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void materiasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void notasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
