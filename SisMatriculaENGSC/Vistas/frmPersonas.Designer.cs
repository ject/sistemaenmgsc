﻿namespace SisMatriculaENGSC.Vistas
{
    partial class frmPersonas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPersonas));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNacionalidad = new System.Windows.Forms.TextBox();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.opcionH = new System.Windows.Forms.RadioButton();
            this.opcionE = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.opcionM = new System.Windows.Forms.RadioButton();
            this.opcionF = new System.Windows.Forms.RadioButton();
            this.txtFechaNac = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtApellido2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtApellido1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombre2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombre1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIdAlumno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboDeptoNac = new System.Windows.Forms.ComboBox();
            this.btnBuscarEncargado = new System.Windows.Forms.Button();
            this.comboParentezco = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEncargado = new System.Windows.Forms.TextBox();
            this.txtEncargadoAlumno = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDistanciaAlumno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.yxyIngresoEncargado = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtProfecionEncargado = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtObsCatedratico = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboEstadoCivilCatedra = new System.Windows.Forms.ComboBox();
            this.txtProfecionCatedratico = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtEmailCatedratico = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNacionalidad);
            this.groupBox1.Controls.Add(this.txtSexo);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.txtFechaNac);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtTelefono);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDireccion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtApellido2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtApellido1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtNombre2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNombre1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtIdAlumno);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 363);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // txtNacionalidad
            // 
            this.txtNacionalidad.Enabled = false;
            this.txtNacionalidad.Location = new System.Drawing.Point(106, 306);
            this.txtNacionalidad.Name = "txtNacionalidad";
            this.txtNacionalidad.Size = new System.Drawing.Size(66, 20);
            this.txtNacionalidad.TabIndex = 29;
            // 
            // txtSexo
            // 
            this.txtSexo.Enabled = false;
            this.txtSexo.Location = new System.Drawing.Point(106, 255);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(49, 20);
            this.txtSexo.TabIndex = 28;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.opcionH);
            this.groupBox4.Controls.Add(this.opcionE);
            this.groupBox4.Location = new System.Drawing.Point(180, 290);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(169, 50);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            // 
            // opcionH
            // 
            this.opcionH.AutoSize = true;
            this.opcionH.Enabled = false;
            this.opcionH.Location = new System.Drawing.Point(6, 19);
            this.opcionH.Name = "opcionH";
            this.opcionH.Size = new System.Drawing.Size(90, 17);
            this.opcionH.TabIndex = 24;
            this.opcionH.TabStop = true;
            this.opcionH.Text = "Hondureño(a)";
            this.opcionH.UseVisualStyleBackColor = true;
            // 
            // opcionE
            // 
            this.opcionE.AutoSize = true;
            this.opcionE.Enabled = false;
            this.opcionE.Location = new System.Drawing.Point(96, 19);
            this.opcionE.Name = "opcionE";
            this.opcionE.Size = new System.Drawing.Size(76, 17);
            this.opcionE.TabIndex = 25;
            this.opcionE.TabStop = true;
            this.opcionE.Text = "Extrangero";
            this.opcionE.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.opcionM);
            this.groupBox3.Controls.Add(this.opcionF);
            this.groupBox3.Location = new System.Drawing.Point(165, 236);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(169, 48);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // opcionM
            // 
            this.opcionM.AutoSize = true;
            this.opcionM.Enabled = false;
            this.opcionM.Location = new System.Drawing.Point(6, 19);
            this.opcionM.Name = "opcionM";
            this.opcionM.Size = new System.Drawing.Size(73, 17);
            this.opcionM.TabIndex = 22;
            this.opcionM.TabStop = true;
            this.opcionM.Text = "Masculino";
            this.opcionM.UseVisualStyleBackColor = true;
            // 
            // opcionF
            // 
            this.opcionF.AutoSize = true;
            this.opcionF.Enabled = false;
            this.opcionF.Location = new System.Drawing.Point(97, 19);
            this.opcionF.Name = "opcionF";
            this.opcionF.Size = new System.Drawing.Size(71, 17);
            this.opcionF.TabIndex = 23;
            this.opcionF.TabStop = true;
            this.opcionF.Text = "Femenino";
            this.opcionF.UseVisualStyleBackColor = true;
            // 
            // txtFechaNac
            // 
            this.txtFechaNac.Enabled = false;
            this.txtFechaNac.Location = new System.Drawing.Point(131, 183);
            this.txtFechaNac.Name = "txtFechaNac";
            this.txtFechaNac.Size = new System.Drawing.Size(163, 20);
            this.txtFechaNac.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 313);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Nacionalidad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 259);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Sexo";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Enabled = false;
            this.txtTelefono.Location = new System.Drawing.Point(131, 210);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(163, 20);
            this.txtTelefono.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Telefono";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Fecha de Nacimiento";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Enabled = false;
            this.txtDireccion.Location = new System.Drawing.Point(130, 157);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(163, 20);
            this.txtDireccion.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Direccion";
            // 
            // txtApellido2
            // 
            this.txtApellido2.Enabled = false;
            this.txtApellido2.Location = new System.Drawing.Point(130, 131);
            this.txtApellido2.Name = "txtApellido2";
            this.txtApellido2.Size = new System.Drawing.Size(163, 20);
            this.txtApellido2.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Segundo Apellido";
            // 
            // txtApellido1
            // 
            this.txtApellido1.Enabled = false;
            this.txtApellido1.Location = new System.Drawing.Point(130, 105);
            this.txtApellido1.Name = "txtApellido1";
            this.txtApellido1.Size = new System.Drawing.Size(163, 20);
            this.txtApellido1.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Primer apellido";
            // 
            // txtNombre2
            // 
            this.txtNombre2.Enabled = false;
            this.txtNombre2.Location = new System.Drawing.Point(130, 79);
            this.txtNombre2.Name = "txtNombre2";
            this.txtNombre2.Size = new System.Drawing.Size(163, 20);
            this.txtNombre2.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Segundo Nombre";
            // 
            // txtNombre1
            // 
            this.txtNombre1.Enabled = false;
            this.txtNombre1.Location = new System.Drawing.Point(130, 53);
            this.txtNombre1.Name = "txtNombre1";
            this.txtNombre1.Size = new System.Drawing.Size(163, 20);
            this.txtNombre1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Primer Nombre";
            // 
            // txtIdAlumno
            // 
            this.txtIdAlumno.Enabled = false;
            this.txtIdAlumno.Location = new System.Drawing.Point(130, 27);
            this.txtIdAlumno.Name = "txtIdAlumno";
            this.txtIdAlumno.Size = new System.Drawing.Size(163, 20);
            this.txtIdAlumno.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero Identidad";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(408, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(318, 175);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(310, 149);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Alumno";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(310, 149);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Encargado";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(310, 149);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Catedratico";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboDeptoNac);
            this.groupBox2.Controls.Add(this.btnBuscarEncargado);
            this.groupBox2.Controls.Add(this.comboParentezco);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtEncargado);
            this.groupBox2.Controls.Add(this.txtEncargadoAlumno);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtDistanciaAlumno);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(9, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(298, 151);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos del Alumno";
            // 
            // comboDeptoNac
            // 
            this.comboDeptoNac.Enabled = false;
            this.comboDeptoNac.FormattingEnabled = true;
            this.comboDeptoNac.Items.AddRange(new object[] {
            "Atlántida",
            "Choluteca",
            "Colon",
            "Comayagua",
            "Copan",
            "Cortes",
            "El Paraíso",
            "Francisco Morazán",
            "Gracias a Dios",
            "Intibucá",
            "Islas de la Bahía",
            "La Paz",
            "Lempira",
            "Ocotepeque",
            "Olancho",
            "Santa Bárbara",
            "Valle",
            "Yoro",
            "Otro"});
            this.comboDeptoNac.Location = new System.Drawing.Point(133, 54);
            this.comboDeptoNac.Name = "comboDeptoNac";
            this.comboDeptoNac.Size = new System.Drawing.Size(162, 21);
            this.comboDeptoNac.TabIndex = 34;
            this.comboDeptoNac.Text = "Seleccione...";
            // 
            // btnBuscarEncargado
            // 
            this.btnBuscarEncargado.Enabled = false;
            this.btnBuscarEncargado.Location = new System.Drawing.Point(278, 79);
            this.btnBuscarEncargado.Name = "btnBuscarEncargado";
            this.btnBuscarEncargado.Size = new System.Drawing.Size(28, 19);
            this.btnBuscarEncargado.TabIndex = 33;
            this.btnBuscarEncargado.Text = "...";
            this.btnBuscarEncargado.UseVisualStyleBackColor = true;
            // 
            // comboParentezco
            // 
            this.comboParentezco.Enabled = false;
            this.comboParentezco.FormattingEnabled = true;
            this.comboParentezco.Items.AddRange(new object[] {
            "Padre",
            "Madre",
            "Tio(a)",
            "Abuelo(a)",
            "Tutor(ra)",
            "Otro"});
            this.comboParentezco.Location = new System.Drawing.Point(133, 105);
            this.comboParentezco.Name = "comboParentezco";
            this.comboParentezco.Size = new System.Drawing.Size(121, 21);
            this.comboParentezco.TabIndex = 32;
            this.comboParentezco.Text = "Seleccione...";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 112);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Parentezco";
            // 
            // txtEncargado
            // 
            this.txtEncargado.Enabled = false;
            this.txtEncargado.Location = new System.Drawing.Point(133, 79);
            this.txtEncargado.Name = "txtEncargado";
            this.txtEncargado.Size = new System.Drawing.Size(138, 20);
            this.txtEncargado.TabIndex = 29;
            // 
            // txtEncargadoAlumno
            // 
            this.txtEncargadoAlumno.AutoSize = true;
            this.txtEncargadoAlumno.Location = new System.Drawing.Point(12, 86);
            this.txtEncargadoAlumno.Name = "txtEncargadoAlumno";
            this.txtEncargadoAlumno.Size = new System.Drawing.Size(59, 13);
            this.txtEncargadoAlumno.TabIndex = 28;
            this.txtEncargadoAlumno.Text = "Encargado";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Depto. de Nacimiento";
            // 
            // txtDistanciaAlumno
            // 
            this.txtDistanciaAlumno.Enabled = false;
            this.txtDistanciaAlumno.Location = new System.Drawing.Point(133, 27);
            this.txtDistanciaAlumno.Name = "txtDistanciaAlumno";
            this.txtDistanciaAlumno.Size = new System.Drawing.Size(162, 20);
            this.txtDistanciaAlumno.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Distancia Casa-Normal";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.yxyIngresoEncargado);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.txtProfecionEncargado);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Location = new System.Drawing.Point(3, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(301, 120);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Datos del Encargado";
            // 
            // yxyIngresoEncargado
            // 
            this.yxyIngresoEncargado.Location = new System.Drawing.Point(132, 74);
            this.yxyIngresoEncargado.Name = "yxyIngresoEncargado";
            this.yxyIngresoEncargado.Size = new System.Drawing.Size(163, 20);
            this.yxyIngresoEncargado.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Ingreso Mensual";
            // 
            // txtProfecionEncargado
            // 
            this.txtProfecionEncargado.Location = new System.Drawing.Point(132, 48);
            this.txtProfecionEncargado.Name = "txtProfecionEncargado";
            this.txtProfecionEncargado.Size = new System.Drawing.Size(163, 20);
            this.txtProfecionEncargado.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "Profecion";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtObsCatedratico);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.comboEstadoCivilCatedra);
            this.groupBox6.Controls.Add(this.txtProfecionCatedratico);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.txtEmailCatedratico);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Location = new System.Drawing.Point(3, 5);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(304, 139);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Datos del Catedratico";
            // 
            // txtObsCatedratico
            // 
            this.txtObsCatedratico.Location = new System.Drawing.Point(132, 105);
            this.txtObsCatedratico.Name = "txtObsCatedratico";
            this.txtObsCatedratico.Size = new System.Drawing.Size(163, 20);
            this.txtObsCatedratico.TabIndex = 32;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Observacion";
            // 
            // comboEstadoCivilCatedra
            // 
            this.comboEstadoCivilCatedra.FormattingEnabled = true;
            this.comboEstadoCivilCatedra.Items.AddRange(new object[] {
            "Soltero(a)",
            "Casado(a)",
            "Viudo(a)",
            "Divorciado(a)",
            "Otro"});
            this.comboEstadoCivilCatedra.Location = new System.Drawing.Point(132, 27);
            this.comboEstadoCivilCatedra.Name = "comboEstadoCivilCatedra";
            this.comboEstadoCivilCatedra.Size = new System.Drawing.Size(121, 21);
            this.comboEstadoCivilCatedra.TabIndex = 30;
            this.comboEstadoCivilCatedra.Text = "Seleccione";
            // 
            // txtProfecionCatedratico
            // 
            this.txtProfecionCatedratico.Location = new System.Drawing.Point(132, 79);
            this.txtProfecionCatedratico.Name = "txtProfecionCatedratico";
            this.txtProfecionCatedratico.Size = new System.Drawing.Size(163, 20);
            this.txtProfecionCatedratico.TabIndex = 29;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 86);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Profecion";
            // 
            // txtEmailCatedratico
            // 
            this.txtEmailCatedratico.Location = new System.Drawing.Point(132, 53);
            this.txtEmailCatedratico.Name = "txtEmailCatedratico";
            this.txtEmailCatedratico.Size = new System.Drawing.Size(163, 20);
            this.txtEmailCatedratico.TabIndex = 27;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 60);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "Email";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Estado Civil";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(589, 207);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(95, 57);
            this.btnGuardar.TabIndex = 38;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(430, 207);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(95, 57);
            this.btnNuevo.TabIndex = 37;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(132, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(163, 20);
            this.textBox1.TabIndex = 31;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Codigo Encargado";
            // 
            // frmPersonas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(745, 387);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPersonas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Personas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNacionalidad;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton opcionH;
        private System.Windows.Forms.RadioButton opcionE;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton opcionM;
        private System.Windows.Forms.RadioButton opcionF;
        private System.Windows.Forms.DateTimePicker txtFechaNac;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtApellido2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtApellido1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombre2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombre1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIdAlumno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboDeptoNac;
        private System.Windows.Forms.Button btnBuscarEncargado;
        private System.Windows.Forms.ComboBox comboParentezco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtEncargado;
        private System.Windows.Forms.Label txtEncargadoAlumno;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDistanciaAlumno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox yxyIngresoEncargado;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtProfecionEncargado;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtObsCatedratico;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboEstadoCivilCatedra;
        private System.Windows.Forms.TextBox txtProfecionCatedratico;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtEmailCatedratico;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
    }
}