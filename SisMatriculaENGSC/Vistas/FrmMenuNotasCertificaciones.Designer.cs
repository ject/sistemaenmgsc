﻿namespace SisMatriculaENGSC.Vistas
{
    partial class frmMenuNotasCertificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenuNotasCertificaciones));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.materiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.combosDeMateriasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDelSistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cambiarModuloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moduloMatriculaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moduloNotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSeciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblFechaHora = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCertificados = new System.Windows.Forms.Button();
            this.btnNotas = new System.Windows.Forms.Button();
            this.btnMaterias = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.materiasToolStripMenuItem,
            this.herramientasToolStripMenuItem,
            this.usuariosToolStripMenuItem,
            this.acercaDelSistemaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(902, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // materiasToolStripMenuItem
            // 
            this.materiasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.combosDeMateriasToolStripMenuItem,
            this.notasToolStripMenuItem});
            this.materiasToolStripMenuItem.Name = "materiasToolStripMenuItem";
            this.materiasToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.materiasToolStripMenuItem.Text = "Materias";
            this.materiasToolStripMenuItem.Click += new System.EventHandler(this.materiasToolStripMenuItem_Click);
            // 
            // combosDeMateriasToolStripMenuItem
            // 
            this.combosDeMateriasToolStripMenuItem.Name = "combosDeMateriasToolStripMenuItem";
            this.combosDeMateriasToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.combosDeMateriasToolStripMenuItem.Text = "Combos de Materias";
            this.combosDeMateriasToolStripMenuItem.Click += new System.EventHandler(this.combosDeMateriasToolStripMenuItem_Click);
            // 
            // notasToolStripMenuItem
            // 
            this.notasToolStripMenuItem.Name = "notasToolStripMenuItem";
            this.notasToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.notasToolStripMenuItem.Text = "Notas";
            this.notasToolStripMenuItem.Click += new System.EventHandler(this.notasToolStripMenuItem_Click);
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.herramientasToolStripMenuItem.Text = "Herramientas";
            this.herramientasToolStripMenuItem.Click += new System.EventHandler(this.herramientasToolStripMenuItem_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.usuariosToolStripMenuItem_Click);
            // 
            // acercaDelSistemaToolStripMenuItem
            // 
            this.acercaDelSistemaToolStripMenuItem.Name = "acercaDelSistemaToolStripMenuItem";
            this.acercaDelSistemaToolStripMenuItem.Size = new System.Drawing.Size(118, 20);
            this.acercaDelSistemaToolStripMenuItem.Text = "Acerca del Sistema";
            this.acercaDelSistemaToolStripMenuItem.Click += new System.EventHandler(this.acercaDelSistemaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.cambiarModuloToolStripMenuItem,
            this.cerrarSeciToolStripMenuItem,
            this.salirToolStripMenuItem1});
            this.toolStripMenuItem1.Image = global::SisMatriculaENGSC.Properties.Resources.usuario1;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(28, 20);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(161, 6);
            // 
            // cambiarModuloToolStripMenuItem
            // 
            this.cambiarModuloToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moduloMatriculaToolStripMenuItem,
            this.moduloNotasToolStripMenuItem});
            this.cambiarModuloToolStripMenuItem.Name = "cambiarModuloToolStripMenuItem";
            this.cambiarModuloToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cambiarModuloToolStripMenuItem.Text = "Cambiar Modulo";
            this.cambiarModuloToolStripMenuItem.Click += new System.EventHandler(this.cambiarModuloToolStripMenuItem_Click);
            // 
            // moduloMatriculaToolStripMenuItem
            // 
            this.moduloMatriculaToolStripMenuItem.Name = "moduloMatriculaToolStripMenuItem";
            this.moduloMatriculaToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.moduloMatriculaToolStripMenuItem.Text = "Modulo Matricula";
            this.moduloMatriculaToolStripMenuItem.Click += new System.EventHandler(this.moduloMatriculaToolStripMenuItem_Click);
            // 
            // moduloNotasToolStripMenuItem
            // 
            this.moduloNotasToolStripMenuItem.Name = "moduloNotasToolStripMenuItem";
            this.moduloNotasToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.moduloNotasToolStripMenuItem.Text = "Modulo Notas";
            this.moduloNotasToolStripMenuItem.Click += new System.EventHandler(this.moduloNotasToolStripMenuItem_Click);
            // 
            // cerrarSeciToolStripMenuItem
            // 
            this.cerrarSeciToolStripMenuItem.Name = "cerrarSeciToolStripMenuItem";
            this.cerrarSeciToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cerrarSeciToolStripMenuItem.Text = "Cerrar Sesión";
            this.cerrarSeciToolStripMenuItem.Click += new System.EventHandler(this.cerrarSeciToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem1
            // 
            this.salirToolStripMenuItem1.Name = "salirToolStripMenuItem1";
            this.salirToolStripMenuItem1.Size = new System.Drawing.Size(164, 22);
            this.salirToolStripMenuItem1.Text = "Salir";
            this.salirToolStripMenuItem1.Click += new System.EventHandler(this.salirToolStripMenuItem1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblUsuario,
            this.lblFechaHora});
            this.statusStrip1.Location = new System.Drawing.Point(0, 618);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(902, 22);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblUsuario
            // 
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(39, 17);
            this.lblUsuario.Text = "Status";
            this.lblUsuario.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // lblFechaHora
            // 
            this.lblFechaHora.Name = "lblFechaHora";
            this.lblFechaHora.Size = new System.Drawing.Size(67, 17);
            this.lblFechaHora.Text = "Fecha Hora";
            this.lblFechaHora.Click += new System.EventHandler(this.lblFechaHora_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnCertificados
            // 
            this.btnCertificados.AccessibleDescription = "";
            this.btnCertificados.AccessibleName = "";
            this.btnCertificados.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.reports;
            this.btnCertificados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCertificados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCertificados.FlatAppearance.BorderSize = 0;
            this.btnCertificados.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnCertificados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCertificados.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCertificados.ForeColor = System.Drawing.Color.Maroon;
            this.btnCertificados.Location = new System.Drawing.Point(796, 343);
            this.btnCertificados.Name = "btnCertificados";
            this.btnCertificados.Size = new System.Drawing.Size(218, 219);
            this.btnCertificados.TabIndex = 8;
            this.btnCertificados.Tag = "";
            this.btnCertificados.Text = "Certificados";
            this.btnCertificados.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnCertificados, "Generar o ver  reportes");
            this.btnCertificados.UseVisualStyleBackColor = true;
            this.btnCertificados.Click += new System.EventHandler(this.btnReportes_Click);
            // 
            // btnNotas
            // 
            this.btnNotas.AccessibleDescription = "";
            this.btnNotas.AccessibleName = "";
            this.btnNotas.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.notas;
            this.btnNotas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNotas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNotas.FlatAppearance.BorderSize = 0;
            this.btnNotas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnNotas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNotas.ForeColor = System.Drawing.Color.Maroon;
            this.btnNotas.Location = new System.Drawing.Point(528, 343);
            this.btnNotas.Name = "btnNotas";
            this.btnNotas.Size = new System.Drawing.Size(218, 219);
            this.btnNotas.TabIndex = 6;
            this.btnNotas.Tag = "";
            this.btnNotas.Text = "Notas";
            this.btnNotas.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnNotas, "Administrar Notas");
            this.btnNotas.UseVisualStyleBackColor = true;
            this.btnNotas.Click += new System.EventHandler(this.btnNotas_Click);
            // 
            // btnMaterias
            // 
            this.btnMaterias.BackgroundImage = global::SisMatriculaENGSC.Properties.Resources.materias;
            this.btnMaterias.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMaterias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMaterias.FlatAppearance.BorderSize = 0;
            this.btnMaterias.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMaterias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaterias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaterias.ForeColor = System.Drawing.Color.Maroon;
            this.btnMaterias.Location = new System.Drawing.Point(259, 343);
            this.btnMaterias.Name = "btnMaterias";
            this.btnMaterias.Size = new System.Drawing.Size(218, 219);
            this.btnMaterias.TabIndex = 5;
            this.btnMaterias.Tag = "";
            this.btnMaterias.Text = "Materias";
            this.btnMaterias.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.btnMaterias, "Administrar Materias");
            this.btnMaterias.UseVisualStyleBackColor = true;
            this.btnMaterias.Click += new System.EventHandler(this.btnMaterias_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SisMatriculaENGSC.Properties.Resources.Logo_normal;
            this.pictureBox1.Location = new System.Drawing.Point(475, 68);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(322, 269);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Stencil", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(348, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(554, 40);
            this.label1.TabIndex = 20;
            this.label1.Text = "Modulo de Notas y Certificados";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(69, 20);
            this.toolStripMenuItem2.Text = "Ayuda";
            // 
            // frmMenuNotasCertificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(902, 640);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnCertificados);
            this.Controls.Add(this.btnNotas);
            this.Controls.Add(this.btnMaterias);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenuNotasCertificaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Notas y Certificaciones";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem materiasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem combosDeMateriasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDelSistemaToolStripMenuItem;
        private System.Windows.Forms.Button btnNotas;
        private System.Windows.Forms.Button btnMaterias;
        private System.Windows.Forms.Button btnCertificados;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblUsuario;
        private System.Windows.Forms.ToolStripStatusLabel lblFechaHora;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem cambiarModuloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSeciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem moduloMatriculaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moduloNotasToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    }
}