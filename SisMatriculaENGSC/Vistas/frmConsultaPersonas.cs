﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmConsultaPersonas : Form
    {
        SqlConnection cnn = Datos.Conexion.getConnection();
        SqlCommand comando;
        SqlDataAdapter da;
        DataTable dt;

        private void CargarPersonas(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarAlumnos(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select A.codPersona,A.primerNombre,A.segundoNombre,A.primerApellido,A.segundoApellido,A.telefonoPersona,A.direccionPersona,A.fechaNacimiento,A.sexo,A.nacionalidad,A.tipoPersona, B.codEncargado,B.distanciaCasa,B.departamentoNacimiento,B.parentesco from tblPersona A,tblAlumno B where A.codPersona=B.codPersona", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CargarEncargados(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select A.codPersona,A.primerNombre,A.segundoNombre,A.primerApellido,A.segundoApellido,A.telefonoPersona,A.direccionPersona,A.fechaNacimiento,A.sexo,A.nacionalidad,A.tipoPersona, B.profesionEncargado,B.ingresoMensual from tblPersona A,tblEncargado B where A.codPersona=B.codPersona", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarCategraticos(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select A.codPersona,A.primerNombre,A.segundoNombre,A.primerApellido,A.segundoApellido,A.telefonoPersona,A.direccionPersona,A.fechaNacimiento,A.sexo,A.nacionalidad,A.tipoPersona, B.estadoCivil,B.profesionCatedratico,B.emailCatedratico,B.observacion from tblPersona A,tblCatedratico B where A.codPersona=B.codPersona", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        public frmConsultaPersonas()
        {
            InitializeComponent();
        }

        private void frmConsultaPersonas_Load(object sender, EventArgs e)
        {
            
            if (Datos.Conexion.usuario_role == "Administrador" || Datos.Conexion.usuario_role=="Coordinador")
            {
                checkBoxEditar.Enabled = true;
                checkBoxEliminar.Enabled = true;
                dataGridViewPersonas.Enabled = true;
            }
            dataGridViewPersonas.Columns[0].Visible = false;
            CargarPersonas(dataGridViewPersonas);
            dataGridViewPersonas.Columns[0].HeaderText = "Eliminar";
            dataGridViewPersonas.Columns[1].HeaderText = "Identidad";
            dataGridViewPersonas.Columns[2].HeaderText = "Primer Nombre";
            dataGridViewPersonas.Columns[3].HeaderText = "Segundo Nombre";
            dataGridViewPersonas.Columns[4].HeaderText = "Primer Apellido";
            dataGridViewPersonas.Columns[5].HeaderText = "Segundo Apellido";
            dataGridViewPersonas.Columns[6].HeaderText = "Télefono";
            dataGridViewPersonas.Columns[7].HeaderText = "Dirección";
            dataGridViewPersonas.Columns[8].HeaderText = "Fecha de Nacimiento";
            dataGridViewPersonas.Columns[9].HeaderText = "Sexo";
            dataGridViewPersonas.Columns[10].HeaderText = "Nacionalidad";
            dataGridViewPersonas.Columns[11].HeaderText = "Tipo Persona";

           
            
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }

        private void opcionAlumnos_CheckedChanged(object sender, EventArgs e)
        {
           
            CargarAlumnos(dataGridViewPersonas );
        }

        private void opcionEncargados_CheckedChanged(object sender, EventArgs e)
        {
            CargarEncargados(dataGridViewPersonas);
        }

        private void opcionCatedraticos_CheckedChanged(object sender, EventArgs e)
        {
            CargarCategraticos(dataGridViewPersonas);
        }

        private void opcionTodos_CheckedChanged(object sender, EventArgs e)
        {
            CargarPersonas(dataGridViewPersonas);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (opcionAlumnos.Checked == true)
            {
                foreach (DataGridViewRow row in dataGridViewPersonas.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    if (chk.Selected == true)
                    {
                        cnn.Open();
                        SqlCommand comando = new SqlCommand("Delete from tblPersona where codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                        SqlCommand comando2 = new SqlCommand("Delete from tblAlumno where codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                        comando.ExecuteNonQuery();
                        comando2.ExecuteNonQuery();
                        cnn.Close();
                        CargarPersonas(dataGridViewPersonas);
                        checkBoxEliminar.Checked = false;
                        opcionTodos.Checked = true;
                        opcionTodos.Enabled = true;
                        dataGridViewPersonas.Columns["Eliminar"].Visible = false;
                        MessageBox.Show("Registro Eliminado Correctamente");
                        
                    }

                }
            } 

            
           

            if (opcionEncargados.Checked == true)
            {
                foreach (DataGridViewRow row in dataGridViewPersonas.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    if (chk.Selected == true)
                    {
                        cnn.Open();
                        SqlCommand comando = new SqlCommand("Delete from tblPersona where codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                        SqlCommand comando2 = new SqlCommand("Delete from tblEncargado where codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                        comando.ExecuteNonQuery();
                        comando2.ExecuteNonQuery();
                        cnn.Close();
                        CargarPersonas(dataGridViewPersonas);
                        checkBoxEliminar.Checked = false;
                        opcionTodos.Checked = true;
                        opcionTodos.Enabled = true;
                        dataGridViewPersonas.Columns["Eliminar"].Visible = false;
                        MessageBox.Show("Registro Eliminado Correctamente");

                    }

                }
            }

            if (opcionCatedraticos.Checked == true)
            {
                foreach (DataGridViewRow row in dataGridViewPersonas.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                    if (chk.Selected == true)
                    {
                        cnn.Open();
                        SqlCommand comando = new SqlCommand("Delete from tblPersona where codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                        SqlCommand comando2 = new SqlCommand("Delete from tblCatedratico where codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                        comando.ExecuteNonQuery();
                        comando2.ExecuteNonQuery();
                        cnn.Close();
                        CargarPersonas(dataGridViewPersonas);
                        checkBoxEliminar.Checked = false;
                        opcionTodos.Checked = true;
                        opcionTodos.Enabled = true;
                        dataGridViewPersonas.Columns["Eliminar"].Visible = false;
                        MessageBox.Show("Registro Eliminado Correctamente");

                    }

                }
            }
        }

        private void checkBoxEliminar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxEliminar_Click(object sender, EventArgs e)
        {
            if (checkBoxEliminar.Checked == true)
            {
                dataGridViewPersonas.Columns[0].Visible = true;
                opcionTodos.Checked = false;
                opcionTodos.Enabled = false;
                opcionAlumnos.Checked = true;
                button3.Enabled = true;


            }
            else
            {
                dataGridViewPersonas.Columns[0].Visible = false;
                opcionTodos.Checked = true;
                opcionTodos.Enabled = true;
                button3.Enabled = false;

            }
        }

        private void checkBoxEditar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
           

            if (txtBuscar.Text != "")
            {
                btnBuscar.Enabled = true;
            }
            else
            {
                btnBuscar.Enabled = false ;
            }
        }

        private void btnBuscar_Click_1(object sender, EventArgs e)
        {
            
            opcionTodos.Checked = true;
            if (comboBuscar.Text == "Identidad")
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona where codPersona like '%" + txtBuscar.Text + "%'", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dataGridViewPersonas.DataSource = dt;
            }
            else if (comboBuscar.Text == "Primer Nombre")
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona where PrimerNombre like '%" + txtBuscar.Text + "%'", cnn);
                dt = new DataTable();
                
                da.Fill(dt);
                dataGridViewPersonas.DataSource = dt;
            }
            else if (comboBuscar.Text == "Primer Apellido")
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona where PrimerApellido like'%" + txtBuscar.Text + "%'", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dataGridViewPersonas.DataSource = dt;
            }
            else if (comboBuscar.Text == "Sexo")
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona where sexo like'%" + txtBuscar.Text + "%'", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dataGridViewPersonas.DataSource = dt;
            }
            else if (comboBuscar.Text == "Nacionalidad")
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona where nacionalidad like '%" + txtBuscar.Text + "%'", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dataGridViewPersonas.DataSource = dt;
            }
            else if (comboBuscar.Text == "Tipo de Persona")
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona where tipoPersona like'%" + txtBuscar.Text + "%'", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dataGridViewPersonas.DataSource = dt;
            }
        }

        private void dataGridViewPersonas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void comboBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void dataGridViewPersonas_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void dataGridViewPersonas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


            if (e.RowIndex >= 0)
            {
                Datos.Conexion.varId = dataGridViewPersonas.Rows[dataGridViewPersonas.CurrentRow.Index].Cells[1].Value.ToString();
            }

            this.Close();
            frmAlumnos frm = new frmAlumnos();
            frm.txtEncargado.Text = Datos.Conexion.varId;
            frm.Show();

           
        }
    }
}
