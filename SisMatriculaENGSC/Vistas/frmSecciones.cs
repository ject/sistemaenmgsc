﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace SisMatriculaENGSC.Vistas
{
    public partial class frmSecciones : Form
    {
        SqlConnection cnn = Datos.Conexion.getConnection();
        SqlDataAdapter da;
        DataTable dt;


        

       


        private void CargarMatriculas(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select codPersona,codCarrera,codCurso,codSemestre,condicionIngreso,jornada,fechaMatricula,seccion from tblMatricula  where seccion ='' or seccion IS NULL ", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public frmSecciones()
        {
            InitializeComponent();
        }

        private void frmSecciones_Load(object sender, EventArgs e)
        {
            CargarMatriculas(dataGridViewSecciones);
            
           dataGridViewSecciones.Columns[0].HeaderText = "Seleccionar";
            dataGridViewSecciones.Columns[1].HeaderText = "Codigo del Alumno";
            dataGridViewSecciones.Columns[2].HeaderText = "Carrera";
            dataGridViewSecciones.Columns[3].HeaderText = "Curso";
            dataGridViewSecciones.Columns[4].HeaderText = "Semestre";
            dataGridViewSecciones.Columns[5].HeaderText = "Condicion de Ingreso";
            dataGridViewSecciones.Columns[6].HeaderText = "Jornada";        
            dataGridViewSecciones.Columns[7].HeaderText = "Fecha de Matricula";
            dataGridViewSecciones.Columns[8].HeaderText = "Seccion";
        
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (txtSeccion.Text == "")
            {
                dataGridViewSecciones.Columns[0].Visible = false;
                btnSeccion.Enabled = false;
            }
            else
            {
                dataGridViewSecciones.Columns[0].Visible = true;
                btnSeccion.Text = "Matricular alumnos en la seccion " + txtSeccion.Text;
                btnSeccion.Enabled = true ;
            }
        }

        private void btnSeccion_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewSecciones.Rows)
            {
                DataGridViewCheckBoxCell chk = row.Cells[0] as DataGridViewCheckBoxCell;
                if (Convert.ToBoolean(chk.Value ))
                {                                                    
                    cnn.Open();
                    SqlCommand comando = new SqlCommand("Update tblMatricula set seccion='" + txtSeccion.Text + "' where  codPersona='" + row.Cells[1].Value.ToString() + "'", cnn);
                    comando.ExecuteNonQuery();
                    cnn.Close();
                    CargarMatriculas(dataGridViewSecciones);
                    MessageBox.Show("Alumnos Matriculados en la seccion " + txtSeccion.Text);
                    txtSeccion.Text = "";
                    txtSeccion.Enabled = false ;
                    btnSeccion.Enabled = true;
                    btnSeccion.Text = "Matricular alumnos en una sección";

                }

            }
            
            txtSeccion.Enabled = true;
            btnSeccion.Enabled = false;
        }

        private void txtSeccion_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            Datos.Validaciones sn = new Datos.Validaciones(); 
            sn.soloNumeros(e);



        }
    }
}
