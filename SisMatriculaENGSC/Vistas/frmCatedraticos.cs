﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmCatedraticos : Form
    {
        public void inHabilitar()
        {
            txtIdPersona.Enabled = false;
            txtNombre1.Enabled = false;
            txtNombre2.Enabled = false;
            txtApellido1.Enabled = false;
            txtApellido2.Enabled = false;
            txtDireccion.Enabled = false;
            txtFechaNac.Enabled = false;
            txtTelefono.Enabled = false;
            opcionM.Enabled = false;
            opcionF.Enabled = false;
            opcionH.Enabled = false;
            opcionE.Enabled = false;
            txtProfecionCatedratico.Enabled = false;
            txtEmailCatedratico.Enabled = false;
            txtObsCatedratico.Enabled = false;
            btnNuevo.Enabled = true;
            opcionM.Checked = true;
            opcionH.Checked = true;
            comboEstadoCivilCatedra.Enabled = false;
        }
        public void limpiar()
        {
            txtIdPersona.Text = "";
            txtNombre1.Text = "";
            txtNombre2.Text = "";
            txtApellido1.Text = "";
            txtApellido2.Text = "";
            txtDireccion.Text = "";
            txtFechaNac.Text = "";
            txtTelefono.Text = "";
            txtProfecionCatedratico.Text = "";
            txtEmailCatedratico.Text = "";
            txtObsCatedratico.Text = "";
            btnGuardar.Enabled = false;
            comboEstadoCivilCatedra.SelectedValue  = "";
            opcionM.Checked = true;
            opcionH.Checked = true;

        }
        public void habilitar()
        {
            txtIdPersona.Enabled = true;
            txtNombre1.Enabled = true;
            txtNombre2.Enabled = true;
            txtApellido1.Enabled = true;
            txtApellido2.Enabled = true;
            txtDireccion.Enabled = true;
            txtFechaNac.Enabled = true;
            txtTelefono.Enabled = true;
            opcionM.Enabled = true;
            opcionF.Enabled = true;
            opcionH.Enabled = true;
            opcionE.Enabled = true;
            txtProfecionCatedratico.Enabled = true;
            txtEmailCatedratico.Enabled = true;
            txtObsCatedratico.Enabled = true;
            btnGuardar.Enabled = true;
            btnNuevo.Enabled = false;
            opcionM.Checked = true;
            opcionH.Checked = true;
            comboEstadoCivilCatedra.Enabled = true;





        }
        public frmCatedraticos()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

      

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label15_Click_1(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
            habilitar();
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtIdPersona.Text) ||
               String.IsNullOrEmpty(txtNombre1.Text) ||
               String.IsNullOrEmpty(txtApellido1.Text) ||
               String.IsNullOrEmpty(txtDireccion.Text) ||
               String.IsNullOrEmpty(txtSexo.Text) ||
               String.IsNullOrEmpty(txtNacionalidad.Text) ||
               String.IsNullOrEmpty(txtProfecionCatedratico.Text) ||
               String.IsNullOrEmpty(comboEstadoCivilCatedra.Text)||
               String.IsNullOrEmpty(txtFechaNac.Text))
            {
                MessageBox.Show("Debe llenar todos los campos");
                errorProvider1.SetError(txtIdPersona, "Campo Obligatorio");
                errorProvider1.SetError(txtNombre1, "Campo Obligatorio");
                errorProvider1.SetError(txtApellido1, "Campo Obligatorio");
                errorProvider1.SetError(txtDireccion, "Campo Obligatorio");
                errorProvider1.SetError(txtSexo, "Campo Obligatorio");
                errorProvider1.SetError(txtNacionalidad, "Campo Obligatorio");
                errorProvider1.SetError(txtProfecionCatedratico, "Campo Obligatorio");
                errorProvider1.SetError(comboEstadoCivilCatedra, "Campo Obligatorio");

            }
            else
            {
                SqlConnection cnn = Datos.Conexion.getConnection();

                try
                {
                    cnn.Open();
                    SqlCommand comando = new SqlCommand("Insert into tblPersona values(@Identidad,@Nombre1,@Nombre2,@Apellido1,@Apellido2,@Telefono,@Direccion,@Fechanac,@Sexo,@Nacionalidad,@TipoPersona)", cnn);
                    SqlCommand comando2 = new SqlCommand("Insert into tblCatedratico values(@Identidad,@EstadoCivil,@Profecion,@Email,@Obs)", cnn);
                    comando.Parameters.AddWithValue("Identidad", txtIdPersona.Text);
                    comando.Parameters.AddWithValue("Nombre1", txtNombre1.Text);
                    comando.Parameters.AddWithValue("Nombre2", txtNombre2.Text);
                    comando.Parameters.AddWithValue("Apellido1", txtApellido1.Text);
                    comando.Parameters.AddWithValue("Apellido2", txtApellido2.Text);
                    comando.Parameters.AddWithValue("Telefono", txtTelefono.Text);
                    comando.Parameters.AddWithValue("Direccion", txtDireccion.Text);
                    comando.Parameters.AddWithValue("Fechanac", txtFechaNac.Value);
                    comando.Parameters.AddWithValue("Sexo", txtSexo.Text);
                    comando.Parameters.AddWithValue("Nacionalidad", txtNacionalidad.Text);
                    comando.Parameters.AddWithValue("TipoPersona", txtTipoPersona.Text);

                    comando2.Parameters.AddWithValue("Identidad", txtIdPersona.Text);
                    comando2.Parameters.AddWithValue("EstadoCivil", comboEstadoCivilCatedra.SelectedItem);
                    comando2.Parameters.AddWithValue("Profecion", txtProfecionCatedratico.Text);
                    comando2.Parameters.AddWithValue("Email", txtEmailCatedratico.Text);
                    comando2.Parameters.AddWithValue("Obs", txtObsCatedratico.Text);




                    comando.ExecuteNonQuery();
                    comando2.ExecuteNonQuery();

                    MessageBox.Show("Regsitro guardado con exito");
                    limpiar();
                    inHabilitar();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al Guardar");
                }
                cnn.Close();
            }
        }

        private void opcionM_CheckedChanged_1(object sender, EventArgs e)
        {
            txtSexo.Text = "Masculino";
        }

        private void opcionF_CheckedChanged(object sender, EventArgs e)
        {
            txtSexo.Text = "Femenino";
        }

        private void opcionH_CheckedChanged(object sender, EventArgs e)
        {
            txtNacionalidad.Text = "Hondureño";
        }

        private void opcionE_CheckedChanged(object sender, EventArgs e)
        {
            txtNacionalidad.Text = "Extrangero";
        }

        private void txtIdPersona_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIdPersona_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloNumeros(e);
        }

        private void txtNombre1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtNombre2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtApellido1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtApellido2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloNumeros(e);
        }

        private void txtProfecionCatedratico_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtObsCatedratico_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }
    }
}
