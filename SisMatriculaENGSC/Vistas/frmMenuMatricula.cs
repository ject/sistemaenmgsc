﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmMenuMatricula : Form
    {
        public frmMenuMatricula()
        {
            InitializeComponent();
        }

        private void lblUsuario_Click(object sender, EventArgs e)
        {

        }

        private void frmMenuMatricula_Load(object sender, EventArgs e) 
        {
            if (Datos.Conexion.usuario_role == "Administrador")
            {
                usuariosToolStripMenuItem.Enabled = true;
            }
            if (Datos.Conexion.usuario_role == "Administrador" || Datos.Conexion.usuario_role == "Coordinador")
            {
              btnAlumnos.Enabled = true;
              btnMatriculas.Enabled = true;
              btbCatedraticos.Enabled = true;
              btnEncargados.Enabled = true;
              btnUsuarios.Enabled = true;
            
            }

            lblUsuario.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            labelUserVirtual.Text = "   Logeado como: "+Datos.Conexion.usuarioNombre;        
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblFechaHora.Text = System.DateTime.Now.ToString("   dd/MM/yyyy    hh:mm:ss");
        }

        private void moduloMatriculaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Vistas.frmMenuMatricula frm = new Vistas.frmMenuMatricula();
            frm.Show();
        }

        private void moduloNotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Vistas.frmMenuNotasCertificaciones frm = new Vistas.frmMenuNotasCertificaciones();
            frm.Show();
        }

        private void combosDeMateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vistas.frmCombosMaterias frm = new Vistas.frmCombosMaterias();
            frm.Show();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Vistas.frmUsuarios frm = new Vistas.frmUsuarios();
            frm.Show();
        }

        private   void btnMatriculas_Click(object sender, EventArgs e)
        {
            Vistas.frmMatriculas frm = new Vistas.frmMatriculas();            
            frm.Show();
        }

        private void btnAlumnos_Click(object sender, EventArgs e)
        {
            Vistas.frmAlumnos frm = new Vistas.frmAlumnos();
            frm.Show();
        }

        private void btbCatedraticos_Click(object sender, EventArgs e)
        {
            Vistas.frmCatedraticos frm = new Vistas.frmCatedraticos();
            frm.Show();
        }

        private void btnEncargados_Click(object sender, EventArgs e)
        {
            Vistas.frmEncargados frm = new Vistas.frmEncargados();
            frm.Show();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            Vistas.frmConsultaPersonas frm = new Vistas.frmConsultaPersonas();
            frm.Show();
        }

        private void notasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void herramientasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cerrarSeciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea Cerrar Sesión?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
                Vistas.frmLogin frm2 = new Vistas.frmLogin();
                frm2.Show();

            }
        }

        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea salir completamente del Sistema?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();

            }
        }

        private void lblFechaHora_Click(object sender, EventArgs e)
        {

        }
    }
}
