﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmMatriculas : Form
    {
        SqlConnection cnn = Datos.Conexion.getConnection();
        SqlDataAdapter da;
        DataTable dt;
        


        private void CargarPersonas(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select codPersona,primerNombre,segundoNombre,primerApellido,segundoApellido,telefonoPersona,direccionPersona,fechaNacimiento,sexo,nacionalidad,tipoPersona from tblPersona", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        
        
        private void CargarMatricula(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select (codPersona,codCarrera,codCurso,codSemestre,condicionIngreso,jornada,fechaMatricula,seccion) from tblMatricula", cnn);
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }





       

        public frmMatriculas()
        {
            InitializeComponent();
        }
         public void limpiar()
        {
            comboCarrera.SelectedValue = null;
            comboCurso.SelectedValue = null;
            txtIdAlumno.Text = "";
            opcionM.Checked = true;
            opcionSemestre1.Checked = true;
            comboCondicion.SelectedValue = null;
            txtJornada.Text = "";
            txtSemestre.Text = "";
            btnGuardar.Enabled = false;
        }
        public void inHabilitar()
        {
            txtIdAlumno.Enabled = false;
            comboCarrera.Enabled = false;
            comboCurso.Enabled = false;
            opcionSemestre1.Enabled = false;
            opcionSemestre2.Enabled = false;
            opcionM.Enabled = false;
            opcionV.Enabled = false;
            comboCondicion.Enabled = false;
            comboCondicion.Enabled = false;
            dateMatricula.Enabled = false;
            btnGuardar.Enabled = false;
            btnBuscarAlumno.Enabled = false;
            btnNuevo.Enabled = true;
        }
        public void habilitar()
        {
            txtIdAlumno.Enabled = true;
            comboCarrera.Enabled = true;
            comboCurso.Enabled = true;
            opcionSemestre1.Enabled = true;
            opcionSemestre2.Enabled = true;
            opcionM.Enabled = true;
            opcionV.Enabled = true;
            comboCondicion.Enabled = true;
            comboCondicion.Enabled = true;
            dateMatricula.Enabled = true;
            btnGuardar.Enabled = true;
            btnBuscarAlumno.Enabled = true;
            btnNuevo.Enabled = false;

        }
       

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            habilitar();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMatricula_Load(object sender, EventArgs e)
        {
            
         
            lblAño.Text  = System.DateTime.Now.ToString("Año: yyyy");
        }

        private void btnBuscarEncargado_Click(object sender, EventArgs e)
        {

            tabControl1.SelectedTab = tabPage2;
           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboCarrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboCarrera.SelectedIndex== 0)
            {
               comboCurso.Items.Clear();
               comboCurso.Items.Add("Primero");
               comboCurso.Items.Add("Segundo");
               comboCurso.Items.Add("Tercero");
               txtCarrera.Text = "01";
            }
            else
            {
                comboCurso.Items.Clear();
                comboCurso.Items.Add("Primero");
                comboCurso.Items.Add("Segundo");
                txtCarrera.Text = "02";
            }
        }

        private void radioBuscar_CheckedChanged(object sender, EventArgs e)
        {
            if (radioBuscarA.Checked == true)
            { 
                    btnBuscar.Enabled = true;
                    comboBuscar.Enabled = true;
                    txtBuscar.Enabled = true;
                    btnEditar.Visible = false;
                    btnEliminar.Visible = false;
                    comboBuscar.Items.Clear();
                    comboBuscar.Items.Add("Identidad");
                    comboBuscar.Items.Add("Nombre");
                    comboBuscar.Items.Add("Apellido");

            }
            else
            {
                btnBuscar.Enabled = true;
                comboBuscar.Enabled = true;
                txtBuscar.Enabled = true;
                btnEditar.Visible = true;
                btnEliminar.Visible = true;
                comboBuscar.Items.Clear();
                comboBuscar.Items.Add("Carrera");
                comboBuscar.Items.Add("Curso");
                comboBuscar.Items.Add("Identidad");
                comboBuscar.Items.Add("Semestre");



            }
           

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtIdAlumno.Text) ||
               String.IsNullOrEmpty(txtSemestre.Text) ||
               String.IsNullOrEmpty(txtCarrera.Text) ||
               String.IsNullOrEmpty(txtCurso.Text) ||
               String.IsNullOrEmpty(txtJornada.Text) ||
               

               String.IsNullOrEmpty(dateMatricula.Text))
            {
                MessageBox.Show("Debe llenar todos los campos");
            }
            else
            {
                SqlConnection cnn = Datos.Conexion.getConnection();

                try
                {
                    cnn.Open();
                    SqlCommand comando = new SqlCommand("Insert into tblMatricula values(@codPersona,@codCarrera,@codCurso,@codSemestre,@condicionIngreso,@jornada,@fechaMatricula,@seccion)", cnn);

                    comando.Parameters.AddWithValue("codPersona", txtIdAlumno.Text);
                    comando.Parameters.AddWithValue("codCarrera", txtCarrera.Text);
                    comando.Parameters.AddWithValue("codCurso", txtCurso.Text);
                    comando.Parameters.AddWithValue("codSemestre", txtSemestre.Text);
                    comando.Parameters.AddWithValue("condicionIngreso", comboCondicion.Text);
                    comando.Parameters.AddWithValue("jornada", txtJornada.Text);
                    comando.Parameters.AddWithValue("fechaMatricula", dateMatricula.Value);
                    comando.Parameters.AddWithValue("seccion", txtSeccion.Text);                



                    comando.ExecuteNonQuery();
                   

                    MessageBox.Show("Regsitro guardado con exito");
                    limpiar();
                    inHabilitar();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error al Guardar");
                }
                cnn.Close();
            }
        }

        private void opcionSemestre1_CheckedChanged(object sender, EventArgs e)
        {
            if(opcionSemestre1.Checked)
                {
                    txtSemestre.Text = "1";
                }
        }

        private void opcionSemestre2_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionSemestre2.Checked)
            {
                txtSemestre.Text = "2";
            }
        }

        private void opcionM_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionM.Checked)
            {
                txtJornada.Text = "M";
            }
        }

        private void opcionV_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionV.Checked)
            {
                txtJornada.Text = "V";
            }
        }

        private void gridbusqueda_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboCurso_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboCurso.SelectedIndex == 0)
            {
                txtCurso.Text = "01";
            }
            else if (comboCurso.SelectedIndex == 0)
            {
                
                txtCurso.Text = "02";
            }
            else
            {
                txtCurso.Text = "03";
            }
        }

        private void comboCondicion_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}
