﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmMaterias : Form
    {
        public frmMaterias()
        {
            InitializeComponent();
        }

        public void habilitar()
        {
            txtCodMateria.Enabled = true;
            txtNombreMateria.Enabled = true;
            btnGuardar.Enabled = true;
            btnNuevo.Enabled = false;
        }

        public void deshabilitar()
        {
            txtCodMateria.Enabled = false;
            txtNombreMateria.Enabled = false;
            btnGuardar.Enabled = false;
            btnNuevo.Enabled = true;
        }

        public void habilitarTeclas()
        {
            btnEditarMateria.Enabled = true;
            btnEliminarMateria.Enabled = true;
        }

        public void deshabilitarTeclas()
        {
            btnEditarMateria.Enabled = false;
            btnEliminarMateria.Enabled = false;
        }

        public void limpiar()
        {
            txtCodMateria.Text = "";
            txtNombreMateria.Text = "";
        }

        public void limpiarConsulta()
        {
            comboBuscarPor.Text = "";
            txtBuscar.Text = "";
        }

        SqlDataAdapter da;
        DataTable dt;
        SqlConnection cnn = Datos.Conexion.getConnection();
        private void CargarMaterias(DataGridView dgv)
        {
            try
            {
                da = new SqlDataAdapter("Select codMateria,nombreMateria from tblMateria", cnn);
                dt = new DataTable();

                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void editarMateria()
        {
            String codMateria = txtCodMateria.Text;
            String nombreMateria = txtNombreMateria.Text;
            SqlConnection cnn = Datos.Conexion.getConnection();

            try
            {
                cnn.Open();
                SqlCommand comando = new SqlCommand("Update tblMateria SET nombreMateria = @nombreMateria where codMateria='" + txtCodMateria.Text + "'", cnn);
                //comando.Parameters.AddWithValue("codMateria", txtCodMateria.Text);
                comando.Parameters.AddWithValue("nombreMateria", txtNombreMateria.Text);

                comando.ExecuteNonQuery();

                MessageBox.Show("Registro editado con exito");
                limpiar();
                deshabilitar();
                CargarMaterias(dgMaterias);
                limpiarConsulta();
                deshabilitarTeclas();
                dgMaterias.Columns["Seleccionar"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error al editar");
            }
            cnn.Close();
        }

        public void Buscar(DataGridView dgv)
        {
            if (comboBuscarPor.Text == "Código")
            {
                da = new SqlDataAdapter("Select codMateria,nombreMateria from tblMateria where codMateria='" + txtBuscar.Text + "'", cnn);
                dt = new DataTable();

                da.Fill(dt);
                dgMaterias.DataSource = dt;
            }
            if (comboBuscarPor.Text == "Nombre")
            {
                da = new SqlDataAdapter("Select codMateria,nombreMateria from tblMateria where nombreMateria='" + txtBuscar.Text + "'", cnn);
                dt = new DataTable();

                da.Fill(dt);
                dgMaterias.DataSource = dt;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            habilitar();
        }

        int guardar=0;
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (guardar == 0)
            {
                if (String.IsNullOrEmpty(txtCodMateria.Text) ||
                   String.IsNullOrEmpty(txtNombreMateria.Text))
                {
                    MessageBox.Show("Debe llenar todos los campos");
                }
                else
                {
                    SqlConnection cnn = Datos.Conexion.getConnection();

                    try
                    {
                        cnn.Open();
                        SqlCommand comando = new SqlCommand("Insert into tblMateria values(@codigoMateria,@nombreMateria)", cnn);
                        comando.Parameters.AddWithValue("codigoMateria", txtCodMateria.Text);
                        comando.Parameters.AddWithValue("nombreMateria", txtNombreMateria.Text);

                        comando.ExecuteNonQuery();

                        MessageBox.Show("Registro guardado con exito");
                        limpiar();
                        deshabilitar();
                        CargarMaterias(dgMaterias);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error al Guardar");
                    }
                    cnn.Close();
                }
            }
            else
            {
                editarMateria();
                guardar = 0;
            }
        }

        int opcion;
        public frmMaterias(int pOpcion)
        {
            InitializeComponent();
            opcion = pOpcion;
        }
        private void frmMaterias_Load(object sender, EventArgs e)
        {
            CargarMaterias(dgMaterias);
            dgMaterias.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgMaterias.Columns[1].HeaderText = "Código";
            dgMaterias.Columns[2].HeaderText = "Nombre";
            if (opcion == 1)
            {
                tabControl1.SelectedTab = tabPage1;
            }
            if (opcion == 2)
            {
                tabControl1.SelectedTab = tabPage2;
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            btnEliminarMateria.Enabled = false;
            dgMaterias.Columns["Seleccionar"].Visible = true;
            foreach (DataGridViewRow row in dgMaterias.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Selected == true)
                {
                    txtCodMateria.Text = dgMaterias.Rows[dgMaterias.CurrentRow.Index].Cells[1].Value.ToString();
                    txtNombreMateria.Text = dgMaterias.Rows[dgMaterias.CurrentRow.Index].Cells[2].Value.ToString();
                    habilitar();
                    deshabilitarTeclas();
                    limpiarConsulta();
                    tabControl1.SelectedTab = tabPage1;
                    txtCodMateria.Enabled = false;
                    guardar = 1;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (opcion == 2)
            {
                btnSeleccionar.Enabled = true;
                Buscar(dgMaterias);
            }
            else
            {
                Buscar(dgMaterias);
                habilitarTeclas();
            }
        }
        
        private void btnEliminarMateria_Click(object sender, EventArgs e)
        {
            btnEditarMateria.Enabled = false;
            dgMaterias.Columns["Seleccionar"].Visible = true;
            foreach (DataGridViewRow row in dgMaterias.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Selected == true)
                {
                    cnn.Open();
                    SqlCommand comando = new SqlCommand("Delete from tblMateria where codMateria='" + row.Cells[1].Value.ToString() + "'", cnn);
                    comando.ExecuteNonQuery();
                    cnn.Close();
                    CargarMaterias(dgMaterias);
                    dgMaterias.Columns["Seleccionar"].Visible = false;
                    MessageBox.Show("Registro Eliminado Correctamente");
                    deshabilitarTeclas();
                    limpiarConsulta();
                }
            }
        }

        private void btnVerConsulta_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            dgMaterias.Columns["Seleccionar"].Visible = true;
            foreach (DataGridViewRow row in dgMaterias.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Selected == true)
                {
                    frmCombosMaterias frm = new frmCombosMaterias();
                    frm.txtMateria.Text = dgMaterias.Rows[dgMaterias.CurrentRow.Index].Cells[2].Value.ToString();
                }
            }
        }

        private void dgMaterias_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
