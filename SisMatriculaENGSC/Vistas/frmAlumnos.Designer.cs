﻿namespace SisMatriculaENGSC.Vistas
{
    partial class frmAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlumnos));
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboDeptoNac = new System.Windows.Forms.ComboBox();
            this.btnBuscarEncargado = new System.Windows.Forms.Button();
            this.comboParentezco = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEncargado = new System.Windows.Forms.TextBox();
            this.txtEncargadoAlumno = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDistanciaAlumno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNacionalidad = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.opcionH = new System.Windows.Forms.RadioButton();
            this.opcionE = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.opcionM = new System.Windows.Forms.RadioButton();
            this.opcionF = new System.Windows.Forms.RadioButton();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.txtFechaNac = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtApellido2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtApellido1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombre2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombre1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIdAlumno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTipoPersona = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtPrueba = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Location = new System.Drawing.Point(609, 285);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(95, 57);
            this.btnGuardar.TabIndex = 44;
            this.btnGuardar.Text = "Guardar";
            this.toolTip1.SetToolTip(this.btnGuardar, "Guardar Registro");
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click_1);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNuevo.Location = new System.Drawing.Point(436, 285);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(95, 57);
            this.btnNuevo.TabIndex = 43;
            this.btnNuevo.Text = "Nuevo";
            this.toolTip1.SetToolTip(this.btnNuevo, "Nuevo Registro");
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click_1);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.comboDeptoNac);
            this.groupBox2.Controls.Add(this.btnBuscarEncargado);
            this.groupBox2.Controls.Add(this.comboParentezco);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtEncargado);
            this.groupBox2.Controls.Add(this.txtEncargadoAlumno);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtDistanciaAlumno);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(386, 58);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 151);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Otros Datos";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(268, 29);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Km";
            // 
            // comboDeptoNac
            // 
            this.comboDeptoNac.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDeptoNac.Enabled = false;
            this.comboDeptoNac.FormattingEnabled = true;
            this.comboDeptoNac.Items.AddRange(new object[] {
            "Atlántida",
            "Choluteca",
            "Colon",
            "Comayagua",
            "Copan",
            "Cortes",
            "El Paraíso",
            "Francisco Morazán",
            "Gracias a Dios",
            "Intibucá",
            "Islas de la Bahía",
            "La Paz",
            "Lempira",
            "Ocotepeque",
            "Olancho",
            "Santa Bárbara",
            "Valle",
            "Yoro",
            "Otro"});
            this.comboDeptoNac.Location = new System.Drawing.Point(133, 54);
            this.comboDeptoNac.Name = "comboDeptoNac";
            this.comboDeptoNac.Size = new System.Drawing.Size(129, 21);
            this.comboDeptoNac.TabIndex = 34;
            // 
            // btnBuscarEncargado
            // 
            this.btnBuscarEncargado.Enabled = false;
            this.btnBuscarEncargado.Location = new System.Drawing.Point(274, 78);
            this.btnBuscarEncargado.Name = "btnBuscarEncargado";
            this.btnBuscarEncargado.Size = new System.Drawing.Size(103, 27);
            this.btnBuscarEncargado.TabIndex = 33;
            this.btnBuscarEncargado.Text = "Buscar Encargado";
            this.btnBuscarEncargado.UseVisualStyleBackColor = true;
            this.btnBuscarEncargado.Click += new System.EventHandler(this.btnBuscarEncargado_Click);
            // 
            // comboParentezco
            // 
            this.comboParentezco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboParentezco.Enabled = false;
            this.comboParentezco.FormattingEnabled = true;
            this.comboParentezco.Items.AddRange(new object[] {
            "Padre",
            "Madre",
            "Tio(a)",
            "Abuelo(a)",
            "Tutor(ra)",
            "Otro"});
            this.comboParentezco.Location = new System.Drawing.Point(133, 109);
            this.comboParentezco.Name = "comboParentezco";
            this.comboParentezco.Size = new System.Drawing.Size(129, 21);
            this.comboParentezco.TabIndex = 32;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Parentezco";
            // 
            // txtEncargado
            // 
            this.txtEncargado.Enabled = false;
            this.txtEncargado.Location = new System.Drawing.Point(133, 81);
            this.txtEncargado.Name = "txtEncargado";
            this.txtEncargado.Size = new System.Drawing.Size(129, 20);
            this.txtEncargado.TabIndex = 29;
            // 
            // txtEncargadoAlumno
            // 
            this.txtEncargadoAlumno.AutoSize = true;
            this.txtEncargadoAlumno.Location = new System.Drawing.Point(12, 88);
            this.txtEncargadoAlumno.Name = "txtEncargadoAlumno";
            this.txtEncargadoAlumno.Size = new System.Drawing.Size(59, 13);
            this.txtEncargadoAlumno.TabIndex = 28;
            this.txtEncargadoAlumno.Text = "Encargado";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Depto. de Nacimiento";
            // 
            // txtDistanciaAlumno
            // 
            this.txtDistanciaAlumno.Enabled = false;
            this.txtDistanciaAlumno.Location = new System.Drawing.Point(133, 27);
            this.txtDistanciaAlumno.Name = "txtDistanciaAlumno";
            this.txtDistanciaAlumno.Size = new System.Drawing.Size(129, 20);
            this.txtDistanciaAlumno.TabIndex = 25;
            this.txtDistanciaAlumno.TextChanged += new System.EventHandler(this.txtDistanciaAlumno_TextChanged);
            this.txtDistanciaAlumno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDistanciaAlumno_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Distancia Casa-Normal";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNacionalidad);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.txtSexo);
            this.groupBox1.Controls.Add(this.txtFechaNac);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtTelefono);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDireccion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtApellido2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtApellido1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtNombre2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNombre1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtIdAlumno);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 363);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // txtNacionalidad
            // 
            this.txtNacionalidad.Enabled = false;
            this.txtNacionalidad.Location = new System.Drawing.Point(305, 303);
            this.txtNacionalidad.Name = "txtNacionalidad";
            this.txtNacionalidad.Size = new System.Drawing.Size(49, 20);
            this.txtNacionalidad.TabIndex = 29;
            this.txtNacionalidad.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.opcionH);
            this.groupBox4.Controls.Add(this.opcionE);
            this.groupBox4.Location = new System.Drawing.Point(131, 290);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(169, 50);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            // 
            // opcionH
            // 
            this.opcionH.AutoSize = true;
            this.opcionH.Enabled = false;
            this.opcionH.Location = new System.Drawing.Point(6, 19);
            this.opcionH.Name = "opcionH";
            this.opcionH.Size = new System.Drawing.Size(90, 17);
            this.opcionH.TabIndex = 24;
            this.opcionH.TabStop = true;
            this.opcionH.Text = "Hondureño(a)";
            this.opcionH.UseVisualStyleBackColor = true;
            this.opcionH.CheckedChanged += new System.EventHandler(this.opcionH_CheckedChanged);
            // 
            // opcionE
            // 
            this.opcionE.AutoSize = true;
            this.opcionE.Enabled = false;
            this.opcionE.Location = new System.Drawing.Point(96, 19);
            this.opcionE.Name = "opcionE";
            this.opcionE.Size = new System.Drawing.Size(76, 17);
            this.opcionE.TabIndex = 25;
            this.opcionE.TabStop = true;
            this.opcionE.Text = "Extrangero";
            this.opcionE.UseVisualStyleBackColor = true;
            this.opcionE.CheckedChanged += new System.EventHandler(this.opcionE_CheckedChanged_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.opcionM);
            this.groupBox3.Controls.Add(this.opcionF);
            this.groupBox3.Location = new System.Drawing.Point(131, 236);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(169, 48);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // opcionM
            // 
            this.opcionM.AutoSize = true;
            this.opcionM.Enabled = false;
            this.opcionM.Location = new System.Drawing.Point(6, 19);
            this.opcionM.Name = "opcionM";
            this.opcionM.Size = new System.Drawing.Size(73, 17);
            this.opcionM.TabIndex = 22;
            this.opcionM.TabStop = true;
            this.opcionM.Text = "Masculino";
            this.opcionM.UseVisualStyleBackColor = true;
            this.opcionM.CheckedChanged += new System.EventHandler(this.opcionM_CheckedChanged);
            // 
            // opcionF
            // 
            this.opcionF.AutoSize = true;
            this.opcionF.Enabled = false;
            this.opcionF.Location = new System.Drawing.Point(97, 19);
            this.opcionF.Name = "opcionF";
            this.opcionF.Size = new System.Drawing.Size(71, 17);
            this.opcionF.TabIndex = 23;
            this.opcionF.TabStop = true;
            this.opcionF.Text = "Femenino";
            this.opcionF.UseVisualStyleBackColor = true;
            this.opcionF.CheckedChanged += new System.EventHandler(this.opcionF_CheckedChanged);
            // 
            // txtSexo
            // 
            this.txtSexo.Enabled = false;
            this.txtSexo.Location = new System.Drawing.Point(305, 252);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(49, 20);
            this.txtSexo.TabIndex = 28;
            this.txtSexo.Visible = false;
            this.txtSexo.TextChanged += new System.EventHandler(this.txtSexo_TextChanged);
            // 
            // txtFechaNac
            // 
            this.txtFechaNac.Enabled = false;
            this.txtFechaNac.Location = new System.Drawing.Point(131, 183);
            this.txtFechaNac.Name = "txtFechaNac";
            this.txtFechaNac.Size = new System.Drawing.Size(200, 20);
            this.txtFechaNac.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 313);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Nacionalidad";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 259);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Sexo";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Enabled = false;
            this.txtTelefono.Location = new System.Drawing.Point(131, 210);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(200, 20);
            this.txtTelefono.TabIndex = 15;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Telefono";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Fecha de Nacimiento";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Enabled = false;
            this.txtDireccion.Location = new System.Drawing.Point(130, 157);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(200, 20);
            this.txtDireccion.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Direccion";
            // 
            // txtApellido2
            // 
            this.txtApellido2.Enabled = false;
            this.txtApellido2.Location = new System.Drawing.Point(130, 131);
            this.txtApellido2.Name = "txtApellido2";
            this.txtApellido2.Size = new System.Drawing.Size(200, 20);
            this.txtApellido2.TabIndex = 9;
            this.txtApellido2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellido2_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Segundo Apellido";
            // 
            // txtApellido1
            // 
            this.txtApellido1.Enabled = false;
            this.txtApellido1.Location = new System.Drawing.Point(130, 105);
            this.txtApellido1.Name = "txtApellido1";
            this.txtApellido1.Size = new System.Drawing.Size(200, 20);
            this.txtApellido1.TabIndex = 7;
            this.txtApellido1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellido1_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Primer apellido";
            // 
            // txtNombre2
            // 
            this.txtNombre2.Enabled = false;
            this.txtNombre2.Location = new System.Drawing.Point(130, 79);
            this.txtNombre2.Name = "txtNombre2";
            this.txtNombre2.Size = new System.Drawing.Size(200, 20);
            this.txtNombre2.TabIndex = 5;
            this.txtNombre2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre2_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Segundo Nombre";
            // 
            // txtNombre1
            // 
            this.txtNombre1.Enabled = false;
            this.txtNombre1.Location = new System.Drawing.Point(130, 53);
            this.txtNombre1.Name = "txtNombre1";
            this.txtNombre1.Size = new System.Drawing.Size(200, 20);
            this.txtNombre1.TabIndex = 3;
            this.txtNombre1.TextChanged += new System.EventHandler(this.txtNombre1_TextChanged);
            this.txtNombre1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre1_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Primer Nombre";
            // 
            // txtIdAlumno
            // 
            this.txtIdAlumno.Enabled = false;
            this.txtIdAlumno.Location = new System.Drawing.Point(130, 27);
            this.txtIdAlumno.Name = "txtIdAlumno";
            this.txtIdAlumno.Size = new System.Drawing.Size(200, 20);
            this.txtIdAlumno.TabIndex = 1;
            this.txtIdAlumno.TextChanged += new System.EventHandler(this.txtIdAlumno_TextChanged);
            this.txtIdAlumno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIdAlumno_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero de Identidad";
            // 
            // txtTipoPersona
            // 
            this.txtTipoPersona.Enabled = false;
            this.txtTipoPersona.Location = new System.Drawing.Point(446, 236);
            this.txtTipoPersona.Name = "txtTipoPersona";
            this.txtTipoPersona.Size = new System.Drawing.Size(248, 20);
            this.txtTipoPersona.TabIndex = 45;
            this.txtTipoPersona.Text = "Alumno";
            this.txtTipoPersona.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::SisMatriculaENGSC.Properties.Resources.close;
            this.pictureBox1.Location = new System.Drawing.Point(761, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Cerrar");
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("News706 BT", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(267, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(275, 41);
            this.label15.TabIndex = 55;
            this.label15.Text = "Agregar Alumno";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // txtPrueba
            // 
            this.txtPrueba.Location = new System.Drawing.Point(576, 30);
            this.txtPrueba.Name = "txtPrueba";
            this.txtPrueba.Size = new System.Drawing.Size(100, 20);
            this.txtPrueba.TabIndex = 57;
            // 
            // frmAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(790, 437);
            this.Controls.Add(this.txtPrueba);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtTipoPersona);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAlumnos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alumnos";
            this.Load += new System.EventHandler(this.frmAlumnos_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboDeptoNac;
        private System.Windows.Forms.Button btnBuscarEncargado;
        private System.Windows.Forms.ComboBox comboParentezco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label txtEncargadoAlumno;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDistanciaAlumno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNacionalidad;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton opcionH;
        private System.Windows.Forms.RadioButton opcionE;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton opcionM;
        private System.Windows.Forms.RadioButton opcionF;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.DateTimePicker txtFechaNac;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtApellido2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtApellido1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombre2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombre1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIdAlumno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTipoPersona;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        public System.Windows.Forms.TextBox txtEncargado;
        public System.Windows.Forms.TextBox txtPrueba;


    }
}