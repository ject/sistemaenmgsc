﻿namespace SisMatriculaENGSC.Vistas
{
    partial class frmConsultaPersonas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultaPersonas));
            this.dataGridViewPersonas = new System.Windows.Forms.DataGridView();
            this.eliminar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.opcionTodos = new System.Windows.Forms.RadioButton();
            this.opcionCatedraticos = new System.Windows.Forms.RadioButton();
            this.opcionEncargados = new System.Windows.Forms.RadioButton();
            this.opcionAlumnos = new System.Windows.Forms.RadioButton();
            this.checkBoxEditar = new System.Windows.Forms.CheckBox();
            this.checkBoxEliminar = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBuscar = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtPrueba = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPersonas)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewPersonas
            // 
            this.dataGridViewPersonas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPersonas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.eliminar});
            this.dataGridViewPersonas.Location = new System.Drawing.Point(12, 74);
            this.dataGridViewPersonas.Name = "dataGridViewPersonas";
            this.dataGridViewPersonas.Size = new System.Drawing.Size(1195, 641);
            this.dataGridViewPersonas.TabIndex = 0;
            this.dataGridViewPersonas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPersonas_CellContentClick);
            this.dataGridViewPersonas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPersonas_CellDoubleClick);
            this.dataGridViewPersonas.DoubleClick += new System.EventHandler(this.dataGridViewPersonas_DoubleClick);
            // 
            // eliminar
            // 
            this.eliminar.HeaderText = "Eliminar";
            this.eliminar.Name = "eliminar";
            this.eliminar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.eliminar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.opcionTodos);
            this.groupBox1.Controls.Add(this.opcionCatedraticos);
            this.groupBox1.Controls.Add(this.opcionEncargados);
            this.groupBox1.Controls.Add(this.opcionAlumnos);
            this.groupBox1.Location = new System.Drawing.Point(40, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(421, 45);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consultar";
            // 
            // opcionTodos
            // 
            this.opcionTodos.AutoSize = true;
            this.opcionTodos.Checked = true;
            this.opcionTodos.Location = new System.Drawing.Point(331, 20);
            this.opcionTodos.Name = "opcionTodos";
            this.opcionTodos.Size = new System.Drawing.Size(55, 17);
            this.opcionTodos.TabIndex = 3;
            this.opcionTodos.TabStop = true;
            this.opcionTodos.Text = "Todos";
            this.opcionTodos.UseVisualStyleBackColor = true;
            this.opcionTodos.CheckedChanged += new System.EventHandler(this.opcionTodos_CheckedChanged);
            // 
            // opcionCatedraticos
            // 
            this.opcionCatedraticos.AutoSize = true;
            this.opcionCatedraticos.Location = new System.Drawing.Point(220, 20);
            this.opcionCatedraticos.Name = "opcionCatedraticos";
            this.opcionCatedraticos.Size = new System.Drawing.Size(84, 17);
            this.opcionCatedraticos.TabIndex = 2;
            this.opcionCatedraticos.Text = "Catedraticos";
            this.opcionCatedraticos.UseVisualStyleBackColor = true;
            this.opcionCatedraticos.CheckedChanged += new System.EventHandler(this.opcionCatedraticos_CheckedChanged);
            // 
            // opcionEncargados
            // 
            this.opcionEncargados.AutoSize = true;
            this.opcionEncargados.Location = new System.Drawing.Point(117, 20);
            this.opcionEncargados.Name = "opcionEncargados";
            this.opcionEncargados.Size = new System.Drawing.Size(82, 17);
            this.opcionEncargados.TabIndex = 1;
            this.opcionEncargados.Text = "Encargados";
            this.opcionEncargados.UseVisualStyleBackColor = true;
            this.opcionEncargados.CheckedChanged += new System.EventHandler(this.opcionEncargados_CheckedChanged);
            // 
            // opcionAlumnos
            // 
            this.opcionAlumnos.AutoSize = true;
            this.opcionAlumnos.Location = new System.Drawing.Point(7, 20);
            this.opcionAlumnos.Name = "opcionAlumnos";
            this.opcionAlumnos.Size = new System.Drawing.Size(65, 17);
            this.opcionAlumnos.TabIndex = 0;
            this.opcionAlumnos.Text = "Alumnos";
            this.opcionAlumnos.UseVisualStyleBackColor = true;
            this.opcionAlumnos.CheckedChanged += new System.EventHandler(this.opcionAlumnos_CheckedChanged);
            // 
            // checkBoxEditar
            // 
            this.checkBoxEditar.AutoSize = true;
            this.checkBoxEditar.Enabled = false;
            this.checkBoxEditar.Location = new System.Drawing.Point(256, 742);
            this.checkBoxEditar.Name = "checkBoxEditar";
            this.checkBoxEditar.Size = new System.Drawing.Size(53, 17);
            this.checkBoxEditar.TabIndex = 14;
            this.checkBoxEditar.Text = "Editar";
            this.checkBoxEditar.UseVisualStyleBackColor = true;
            this.checkBoxEditar.CheckedChanged += new System.EventHandler(this.checkBoxEditar_CheckedChanged);
            // 
            // checkBoxEliminar
            // 
            this.checkBoxEliminar.AutoSize = true;
            this.checkBoxEliminar.Enabled = false;
            this.checkBoxEliminar.Location = new System.Drawing.Point(43, 742);
            this.checkBoxEliminar.Name = "checkBoxEliminar";
            this.checkBoxEliminar.Size = new System.Drawing.Size(62, 17);
            this.checkBoxEliminar.TabIndex = 13;
            this.checkBoxEliminar.Text = "Eliminar";
            this.checkBoxEliminar.UseVisualStyleBackColor = true;
            this.checkBoxEliminar.CheckedChanged += new System.EventHandler(this.checkBoxEliminar_CheckedChanged);
            this.checkBoxEliminar.Click += new System.EventHandler(this.checkBoxEliminar_Click);
            // 
            // button3
            // 
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(111, 738);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Eliminar";
            this.toolTip1.SetToolTip(this.button3, "Eliminar Registro");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(324, 736);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Editar";
            this.toolTip1.SetToolTip(this.button2, "Editar Registro");
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBuscar);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnBuscar);
            this.groupBox2.Controls.Add(this.txtBuscar);
            this.groupBox2.Location = new System.Drawing.Point(772, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(463, 45);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Busqueda";
            // 
            // comboBuscar
            // 
            this.comboBuscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBuscar.FormattingEnabled = true;
            this.comboBuscar.Items.AddRange(new object[] {
            "Identidad",
            "Primer Nombre",
            "Primer Apellido",
            "Sexo",
            "Nacionalidad",
            "Tipo de Persona"});
            this.comboBuscar.Location = new System.Drawing.Point(68, 16);
            this.comboBuscar.Name = "comboBuscar";
            this.comboBuscar.Size = new System.Drawing.Size(121, 21);
            this.comboBuscar.TabIndex = 8;
            this.comboBuscar.SelectedIndexChanged += new System.EventHandler(this.comboBuscar_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Buscar por:";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscar.Enabled = false;
            this.btnBuscar.Location = new System.Drawing.Point(382, 14);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "Buscar";
            this.toolTip1.SetToolTip(this.btnBuscar, "Buscar Registro");
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click_1);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(195, 16);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(181, 20);
            this.txtBuscar.TabIndex = 5;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // txtPrueba
            // 
            this.txtPrueba.Location = new System.Drawing.Point(540, 27);
            this.txtPrueba.Name = "txtPrueba";
            this.txtPrueba.Size = new System.Drawing.Size(100, 20);
            this.txtPrueba.TabIndex = 16;
            // 
            // frmConsultaPersonas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1276, 773);
            this.Controls.Add(this.txtPrueba);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.checkBoxEditar);
            this.Controls.Add(this.checkBoxEliminar);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridViewPersonas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConsultaPersonas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta de Personas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmConsultaPersonas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPersonas)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewPersonas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn eliminar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.RadioButton opcionCatedraticos;
        public System.Windows.Forms.RadioButton opcionEncargados;
        public System.Windows.Forms.RadioButton opcionAlumnos;
        public System.Windows.Forms.RadioButton opcionTodos;
        public System.Windows.Forms.CheckBox checkBoxEditar;
        public System.Windows.Forms.CheckBox checkBoxEliminar;
        private System.Windows.Forms.TextBox txtPrueba;

    }
}