﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmAlumnos : Form
    {
        public void inHabilitar()
        {
            txtIdAlumno.Enabled = false;
            txtNombre1.Enabled = false;
            txtNombre2.Enabled = false;
            txtApellido1.Enabled = false;
            txtApellido2.Enabled = false;
            txtDireccion.Enabled = false;
            txtFechaNac.Enabled = false;
            txtTelefono.Enabled = false;
            opcionM.Enabled = false;
            opcionF.Enabled = false;
            opcionH.Enabled = false;
            opcionE.Enabled = false;
            txtDistanciaAlumno.Enabled = false;
            comboDeptoNac.Enabled = false;
            comboDeptoNac.SelectedValue = null ;
            comboParentezco.SelectedValue = null ;
            btnBuscarEncargado.Enabled = false;
            comboParentezco.Enabled = false;
            btnNuevo.Enabled = true;
            opcionM.Checked = true;
            opcionH.Checked = true;
        }
        public void limpiar()
        {
            txtIdAlumno.Text = "";
            txtNombre1.Text = "";
            txtNombre2.Text = "";
            txtApellido1.Text = "";
            txtApellido2.Text = "";
            txtDireccion.Text = "";
            txtFechaNac.Text = "";
            txtTelefono.Text = "";
            txtDistanciaAlumno.Text = "";
            txtEncargado.Text = "";
            btnGuardar.Enabled = false;
            comboDeptoNac.SelectedValue  = null;
            comboParentezco.SelectedValue = null;
            opcionM.Checked = true;
            opcionH.Checked = true;

        }
        public void habilitar()
        {
            txtIdAlumno.Enabled = true;
            txtNombre1.Enabled = true;
            txtNombre2.Enabled = true;
            txtApellido1.Enabled = true;
            txtApellido2.Enabled = true;
            txtDireccion.Enabled = true;
            txtFechaNac.Enabled = true;
            txtTelefono.Enabled = true;
            opcionM.Enabled = true;
            opcionF.Enabled = true;
            opcionH.Enabled = true;
            opcionE.Enabled = true;
            txtDistanciaAlumno.Enabled = true;
            comboDeptoNac.Enabled = true;
            btnBuscarEncargado.Enabled = true;
            comboParentezco.Enabled = true;
            btnGuardar.Enabled = true;
            btnNuevo.Enabled = false;
            opcionM.Checked  = true;
            opcionH.Checked = true;
            comboDeptoNac.SelectedValue = 0;
            comboParentezco.SelectedValue = 0;
           



        }
        public frmAlumnos()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void frmAlumnos_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            habilitar();
        }

        private void comboSexo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionF.Checked)
            {
                txtSexo.Text = "F";
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionH.Checked)
            {
                txtNacionalidad.Text = "Hondureño";
            }
        }
        

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboParentezco_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionM.Checked )
            {
                txtSexo.Text = "M";
            }
        }

        private void opcionE_CheckedChanged(object sender, EventArgs e)
        {
            if (opcionE.Checked)
            {
                txtNacionalidad.Text = "Extrangero";
            }
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtIdAlumno.Text) ||
               String.IsNullOrEmpty(txtNombre1.Text) ||
               String.IsNullOrEmpty(txtApellido1.Text) ||
               String.IsNullOrEmpty(txtDireccion.Text) ||
               String.IsNullOrEmpty(txtSexo.Text) ||
               String.IsNullOrEmpty(txtNacionalidad.Text) ||
               String.IsNullOrEmpty(txtDistanciaAlumno.Text) ||
               String.IsNullOrEmpty(comboDeptoNac.Text) ||
               String.IsNullOrEmpty(txtFechaNac.Text))
            {
                
              MessageBox.Show("Debe llenar todos los campos obligatorios");
              errorProvider1.SetError(txtIdAlumno, "Campo Obligatorio");
              errorProvider1.SetError(txtNombre1, "Campo Obligatorio");
              errorProvider1.SetError(txtApellido1, "Campo Obligatorio");
              errorProvider1.SetError(txtDireccion, "Campo Obligatorio");
              errorProvider1.SetError(txtSexo, "Campo Obligatorio");
              errorProvider1.SetError(txtNacionalidad, "Campo Obligatorio");
              errorProvider1.SetError(txtDistanciaAlumno, "Campo Obligatorio");
              errorProvider1.SetError(txtFechaNac, "Campo Obligatorio");
              errorProvider1.SetError(comboDeptoNac, "Campo Obligatorio");
            }
            else
            {
                SqlConnection cnn = Datos.Conexion.getConnection();

                try
                {
                    cnn.Open();
                    SqlCommand comando = new SqlCommand("Insert into tblPersona values(@Identidad,@Nombre1,@Nombre2,@Apellido1,@Apellido2,@Telefono,@Direccion,@Fechanac,@Sexo,@Nacionalidad,@TipoPersona)", cnn);
                    SqlCommand comando2 = new SqlCommand("Insert into tblAlumno values(@Identidad,@Encargado,@Distancia,@DeptoNac,@Parentezco)", cnn);
                    comando.Parameters.AddWithValue("Identidad", txtIdAlumno.Text);
                    comando.Parameters.AddWithValue("Nombre1", txtNombre1.Text);
                    comando.Parameters.AddWithValue("Nombre2", txtNombre2.Text);
                    comando.Parameters.AddWithValue("Apellido1", txtApellido1.Text);
                    comando.Parameters.AddWithValue("Apellido2", txtApellido2.Text);
                    comando.Parameters.AddWithValue("Telefono", txtTelefono.Text);
                    comando.Parameters.AddWithValue("Direccion", txtDireccion.Text);
                    comando.Parameters.AddWithValue("Fechanac", txtFechaNac.Value );
                    comando.Parameters.AddWithValue("Sexo", txtSexo.Text);
                    comando.Parameters.AddWithValue("Nacionalidad", txtNacionalidad.Text);
                    comando.Parameters.AddWithValue("TipoPersona", txtTipoPersona.Text);
                    comando2.Parameters.AddWithValue("Identidad", txtIdAlumno.Text);
                    comando2.Parameters.AddWithValue("Encargado", txtEncargado.Text);
                    comando2.Parameters.AddWithValue("Distancia", txtDistanciaAlumno.Text);
                    comando2.Parameters.AddWithValue("DeptoNac", comboDeptoNac.SelectedItem);
                    comando2.Parameters.AddWithValue("Parentezco", comboParentezco.SelectedItem);
                    
                    

                    comando.ExecuteNonQuery();
                    comando2.ExecuteNonQuery();

                    MessageBox.Show("Regsitro guardado con exito");
                    limpiar();
                    inHabilitar();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message,"Error al Guardar");
                }
                cnn.Close();
            }
        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
            habilitar();

        }

        private void txtSexo_TextChanged(object sender, EventArgs e)
        {

        }

        private void opcionF_CheckedChanged(object sender, EventArgs e)
        {
            txtSexo.Text = "Femenino";
        }

        private void opcionM_CheckedChanged(object sender, EventArgs e)
        {
            txtSexo.Text = "Masculino";
        }

        private void opcionH_CheckedChanged(object sender, EventArgs e)
        {
            txtNacionalidad.Text = "Hondureño";
        }

        private void opcionE_CheckedChanged_1(object sender, EventArgs e)
        {
            txtNacionalidad.Text = "Extrangero";
        }

        private void btnBuscarEncargado_Click(object sender, EventArgs e)
        {
            frmConsultaPersonas f = new frmConsultaPersonas();
            f.opcionAlumnos.Enabled = false;
            f.opcionCatedraticos.Enabled = false;
            f.opcionEncargados.Enabled = false;
         
            frmConsultaPersonas frm=new frmConsultaPersonas();
            frm.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void txtIdAlumno_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloNumeros(e);
            
        }

        private void txtIdAlumno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNombre1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtNombre1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtNombre2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtApellido1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtApellido2_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloLetras(e);
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloNumeros(e);
        }

        private void txtDistanciaAlumno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDistanciaAlumno_KeyPress(object sender, KeyPressEventArgs e)
        {
            Datos.Validaciones sn = new Datos.Validaciones();
            sn.soloNumeros(e);
        }
            
        }
    }

