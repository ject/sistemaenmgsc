﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmCombosMaterias : Form
    {
        public frmCombosMaterias()
        {
            InitializeComponent();
        }

        private void comboParentezco_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboCarrera.SelectedIndex==0)
            {
                comboCurso.Items.Clear();
                comboCurso.Items.Add("Primero");
                comboCurso.Items.Add("Segundo");
            }
            else
            {
                comboCurso.Items.Clear();
                comboCurso.Items.Add("Primero");
                comboCurso.Items.Add("Segundo");
                comboCurso.Items.Add("Tercero");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            btnVerCombo.Enabled = true;
            btnBuscar.Enabled = true;
            btnGuardar.Enabled = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            btnVerCombo.Enabled = false;
            btnGuardar.Enabled = true;
            btnBuscar.Enabled = false;
            Vistas.frmMaterias frm = new Vistas.frmMaterias(2);
            frm.Show();
        }

        private void btnVerCombo_Click(object sender, EventArgs e)
        {
            btnEliminar.Enabled = true;
            btnEditar.Enabled = true;
            btnBuscar.Enabled = false;
            btnVerCombo.Enabled = false;
        }

        private void dgComboMaterias_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
