﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        public void entrar()
        {
            String usuario = txtUsuario.Text;
            String pass = txtContraseña.Text;
            try
            {
                SqlConnection cnn = Datos.Conexion.getConnection();
                cnn.Open();
                SqlDataReader dr = null;
                SqlCommand consulta = new SqlCommand("select * from tblUsuarios where idUsuario = '" + usuario + "' and passUsuario = '" + pass + "'", cnn);
                consulta.CommandType = CommandType.Text;
                dr = consulta.ExecuteReader();
                if (dr.Read())
                {
                    Datos.Conexion.usuarioNombre = dr["idUsuario"].ToString();
                    //Datos.Conexion.estidianteID = Convert.ToInt32(dr["codigoUsuario"].ToString());
                    Datos.Conexion.usuario_role = dr["tipoUsuario"].ToString();
                    MessageBox.Show("Bienvenido sr(a): " + Datos.Conexion.usuarioNombre);
                    
                    

                    this.Hide();
                    frmModulos f = new frmModulos();
                    f.Show();
                    f.label1.Text = "Bienvenido (" + Datos.Conexion.usuarioNombre + ")";
                    f.Text = "Registro Virtual (" + Datos.Conexion.usuarioNombre + ")";

                    cnn.Close();

                    //sacarDatos();
                }
                else
                {
                    MessageBox.Show("contrasena o usuario incorrecto");
                    cnn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsuario.Text) || string.IsNullOrEmpty(txtContraseña.Text))
            {
                MessageBox.Show("Debe entrar un usuario y una contraseña");
            }
            else
            {
                entrar();
            }
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que desea Cancelar?", "Sistema de Informacion Suazo Córdova", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();

            }
        }
    }
}
