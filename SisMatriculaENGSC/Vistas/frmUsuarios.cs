﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SisMatriculaENGSC.Vistas
{
    public partial class frmUsuarios : Form
    {
       
        SqlCommand comando;
        DataTable dt;
        DataSet ds;

        SqlConnection cnn = Datos.Conexion.getConnection();
        SqlDataAdapter da;
        private void CargarUsuarios(DataGridView dgv)
        {
           
            try
            {
               
                da=new SqlDataAdapter ("Select codUsuario,idUsuario,tipoUsuario from tblUsuarios",cnn);
                dt = new DataTable() ;

                da.Fill(dt);
                dgv.DataSource = dt;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message );
            }
           
            
        }


        public void inHabilitar()
        {
            txtcodUsuario.Enabled = false;
           
            txtUsuario.Enabled = false;
            txtContrasena.Enabled = false;
            txtConfirmarContraseña.Enabled = false;
            comboTipoUsuario.Enabled = false;
            btnGuardar.Enabled = false;
            btnNuevo.Enabled = true;

        }
        public void habilitar()
        {
            txtcodUsuario.Enabled = true;
           
            txtUsuario.Enabled = true;
            txtContrasena.Enabled = true;
            txtConfirmarContraseña.Enabled = true;
            comboTipoUsuario.Enabled = true;
            btnGuardar.Enabled = true;
            btnNuevo.Enabled = false;

        }

        public void limpiar()
        {
            txtcodUsuario.Text = "";
           
            txtUsuario.Text = "";
            txtContrasena.Text = "";
            txtConfirmarContraseña.Text = "";
            comboBuscar.Text="Seleccione...";
            btnNuevo.Enabled = true;
            btnGuardar.Enabled = false;
            
            

        }
        public frmUsuarios()
        {
            InitializeComponent();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

            

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtcodUsuario.Text) ||
              
               String.IsNullOrEmpty(txtUsuario.Text) ||
               String.IsNullOrEmpty(txtContrasena.Text) ||
               String.IsNullOrEmpty(comboTipoUsuario.Text)||
                txtContrasena.Text!=txtConfirmarContraseña.Text )
            {
                MessageBox.Show("Debe llenar correctamente todos los campos y confirmar su contraseña");
            }

           
            else
            {
                SqlConnection cnn = Datos.Conexion.getConnection();

                try
                {
                    cnn.Open();
                    comando = new SqlCommand("Insert into tblUsuarios values(@codUsuario, @Usuario, @Contraseña, @TipoUsuario)", cnn);

                   comando.Parameters.AddWithValue("codUsuario", txtcodUsuario.Text );
                  
                   comando.Parameters.AddWithValue("Usuario", txtUsuario.Text);
                   comando.Parameters.AddWithValue("Contraseña", txtContrasena.Text);
                   comando.Parameters.AddWithValue("TipoUsuario", comboTipoUsuario.Text);
                   
                    comando.ExecuteNonQuery();

                    MessageBox.Show("Regsitro guardado con exito");
                    limpiar();
                    inHabilitar();
                    CargarUsuarios(dataGridViewUsuarios);
                }
               catch (Exception ex)
               {
                    MessageBox.Show(ex.Message);
                } 
                cnn.Close();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

            habilitar();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            

            CargarUsuarios(dataGridViewUsuarios);
            
            
            dataGridViewUsuarios.Columns[0].Visible  = false ;
            dataGridViewUsuarios.Columns[0].HeaderText = "Eliminar";
            dataGridViewUsuarios.Columns[1].HeaderText = "Codigo";
            dataGridViewUsuarios.Columns[2].HeaderText = "Usuario";
            dataGridViewUsuarios.Columns[3].HeaderText = "Tipo de Usuario";
           

        }

        private void button3_Click(object sender, EventArgs e)
        {


            foreach (DataGridViewRow row in dataGridViewUsuarios.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Selected == true)
                {
                    

                        cnn.Open();
                        SqlCommand comando = new SqlCommand("Delete from tblUsuarios where codUsuario='" + row.Cells[1].Value.ToString() + "'", cnn);
                        comando.ExecuteNonQuery();
                        cnn.Close();
                        CargarUsuarios(dataGridViewUsuarios);
                        button3.Enabled = false;
                        MessageBox.Show("Registros Eliminados Correctamente");
                        checkBoxEliminar.Checked = false;
                        dataGridViewUsuarios.Columns["Eliminar"].Visible = false;
                        

                   

                }
                
            }

            

        }

        private void checkBoxEliminar_CheckedChanged(object sender, EventArgs e)
        {
            
            
            

        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void checkBoxEditar_CheckedChanged(object sender, EventArgs e)
        {

            
        }

        private void checkBoxEditar_Click(object sender, EventArgs e)
        {
            if (checkBoxEditar.Checked)
            {
                dataGridViewUsuarios.Enabled = true;
            }
            else
            {
                dataGridViewUsuarios.Enabled = false;
            }
        }

        private void checkBoxEliminar_Click(object sender, EventArgs e)
        {
            if (checkBoxEliminar.Checked)
            {
                dataGridViewUsuarios.Columns[0].Visible = true;
                dataGridViewUsuarios.Enabled = true;
                button3.Enabled = true;
            }
            else
            {
                dataGridViewUsuarios.Columns[0].Visible = false;
                dataGridViewUsuarios.Enabled = false ;
                button3.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           ds =new DataSet();

           
            try
            {
                cnn.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select tblUsuarios, codUsuario,Usuario,TipoUsuario", cnn);
                SqlCommandBuilder  com = new SqlCommandBuilder(da);
                da.Update(ds);
                MessageBox.Show("Editado");


            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message );
            }
           
        }

        private void dataGridViewUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int posicion = dataGridViewUsuarios.CurrentRow.Index;
           
            
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBuscar.Text == "Codigo")
            {
                da = new SqlDataAdapter("Select codUsuario,idUsuario,tipoUsuario from tblUsuarios where codUsuario='" + txtBuscar.Text + "'", cnn);
                dt = new DataTable();

                da.Fill(dt);
                dataGridViewUsuarios.DataSource = dt;
                checkBox1.Checked = false;

            }
            else if (comboBuscar.Text == "Usuario")
            {
                da = new SqlDataAdapter("Select codUsuario,idUsuario,tipoUsuario from tblUsuarios where idUsuario='" + txtBuscar.Text + "'", cnn);
                dt = new DataTable();

                da.Fill(dt);
                dataGridViewUsuarios.DataSource = dt;
                checkBox1.Checked = false;
            }
            else if (comboBuscar.Text == "Tipo de Usuario")
            {
                da = new SqlDataAdapter("Select codUsuario,idUsuario,tipoUsuario from tblUsuarios where tipoUsuario='" + txtBuscar.Text + "'", cnn);
                dt = new DataTable();

                da.Fill(dt);
                dataGridViewUsuarios.DataSource = dt;
                checkBox1.Checked = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked != true)
            {
                btnBuscar.Enabled = true;
                txtBuscar.Enabled = true;
                txtBuscar.Text = "";
                comboBuscar.Enabled = true;
            }
            else
            {
                CargarUsuarios(dataGridViewUsuarios);
                btnBuscar.Enabled = false;
                txtBuscar.Enabled = false;
                comboBuscar.Enabled = false;
            }
            

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
